'use strict';

module.exports = function (grunt) {
    // Project Configuration
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        watch: {
            jade: {
                files: [ 'views/**' ],
                options: {
                    livereload: true,
                },
            },
            js: {
                files: [ 'public/**/*.js', 'modules/**/*.js', 'config/**/*.js' ],
                tasks: [ 'jshint' ],
                options: {
                    livereload: true,
                },
            },
            html: {
                files: [ 'public/views/**' ],
                options: {
                    livereload: true,
                },
            },
            css: {
                files: [ 'public/css/**' ],
                options: {
                    livereload: true
                }
            }
        },
        jshint: {
            all: [ 'gruntfile.js', 'public/js/**/*.js', 'test/mocha/**/*.js', 'test/karma/**/*.js', 'app/**/*.js' ],
            options: {
                jshintrc: '.jshintrc',
                reporter: require('jshint-stylish')
            }
        },
        copy: {
            options: {
                punctuation: ''
            },
            js: {
                files: [
                    { cwd: 'bower_components/jquery', src: [ 'dist/*.js' ], dest: 'public/lib/jquery', expand: true },
                    { cwd: 'bower_components/jquery-ui', src: [ '*.js' ], dest: 'public/lib/jquery-ui', expand: true },
                    {
                        cwd: 'bower_components/jquery.easing', src: [ 'js/*.js' ], dest: 'public/lib/jquery.easing',
                        expand: true
                    },
                    {
                        cwd: 'bower_components/jquery.easy-pie-chart', src: [ 'dist/*.js' ],
                        dest: 'public/lib/jquery.easy-pie-chart', expand: true
                    },
                    {
                        cwd: 'bower_components/chart.js',
                        src: [ 'dist/*.js' ],
                        dest: 'public/lib/chart.js',
                        expand: true
                    },

                    {
                        cwd: 'bower_components/amcharts-stock', src: [ 'dist/**/*.js' ],
                        dest: 'public/lib/amcharts-stock', expand: true
                    },
                    { cwd: 'bower_components/angular', src: [ '*.js' ], dest: 'public/lib/angular', expand: true },
                    {
                        cwd: 'bower_components/angular-route', src: [ '*.js' ],
                        dest: 'public/lib/angular-route', expand: true
                    },
                    {
                        cwd: 'bower_components/slimScroll',
                        src: [ '*.js' ],
                        dest: 'public/lib/slimScroll',
                        expand: true
                    },
                    {
                        cwd: 'bower_components/angular-slimscroll', src: [ '*.js' ],
                        dest: 'public/lib/angular-slimscroll', expand: true
                    },
                    {
                        cwd: 'bower_components/angular-smart-table', src: [ 'dist/*.js' ],
                        dest: 'public/lib/angular-smart-table', expand: true
                    },
                    {
                        cwd: 'bower_components/angular-toastr', src: [ 'dist/*.js', 'dist/*.css' ],
                        dest: 'public/lib/angular-toastr', expand: true
                    },
                    {
                        cwd: 'bower_components/angular-touch', src: [ '*.js' ],
                        dest: 'public/lib/angular-touch', expand: true
                    },
                    {
                        cwd: 'bower_components/angular-ui-sortable', src: [ '*.js' ],
                        dest: 'public/lib/angular-ui-sortable', expand: true
                    },
                    {
                        cwd: 'bower_components/bootstrap/dist', src: [ '**/*.js', '**/*.css', '**/*.map' ],
                        dest: 'public/lib/bootstrap/dist', expand: true
                    },
                    {
                        cwd: 'bower_components/bootstrap-select', src: [ '**/*.js', '**/*.css' ],
                        dest: 'public/lib/bootstrap-select', expand: true
                    },
                    {
                        cwd: 'bower_components/bootstrap-switch', src: [ '**/*.js', '**/*.css' ],
                        dest: 'public/lib/bootstrap-switch', expand: true
                    },
                    {
                        cwd: 'bower_components/bootstrap-tagsinput', src: [ '**/*.js', '**/*.css' ],
                        dest: 'public/lib/bootstrap-tagsinput', expand: true
                    },
                    { cwd: 'bower_components/moment', src: [ '**/*.js' ], dest: 'public/lib/moment', expand: true },
                    {
                        cwd: 'bower_components/fullcalendar', src: [ 'dist/**/*.js', '**/*.css' ],
                        dest: 'public/lib/fullcalendar', expand: true
                    },
                    {
                        cwd: 'bower_components/leaflet', src: [ 'dist/*.js', '**/*.css' ],
                        dest: 'public/lib/leaflet', expand: true
                    },
                    {
                        cwd: 'bower_components/angular-progress-button-styles', src: [ 'dist/**/*.js', '**/*.css' ],
                        dest: 'public/lib/angular-progress-button-styles', expand: true
                    },
                    {
                        cwd: 'bower_components/angular-ui-router', src: [ '**/*.js' ],
                        dest: 'public/lib/angular-ui-router', expand: true
                    },
                    {
                        cwd: 'bower_components/angular-chart.js', src: [ '**/*.js' ],
                        dest: 'public/lib/angular-chart.js', expand: true
                    },
                    {
                        cwd: 'bower_components/chartist', src: [ '**/*.js', '**/*.css', '**/*.map' ],
                        dest: 'public/lib/chartist', expand: true
                    },
                    {
                        cwd: 'bower_components/angular-chartist.js', src: [ '**/*.js' ],
                        dest: 'public/lib/angular-chartist.js', expand: true
                    },
                    {
                        cwd: 'bower_components/eve-raphael', src: [ '**/*.js' ],
                        dest: 'public/lib/eve-raphael', expand: true
                    },
                    { cwd: 'bower_components/raphael', src: [ '**/*.js' ], dest: 'public/lib/raphael', expand: true },
                    { cwd: 'bower_components/mocha', src: [ '**/*.js' ], dest: 'public/lib/mocha', expand: true },
                    {
                        cwd: 'bower_components/morris.js', src: [ '**/*.js', '**/*.css' ],
                        dest: 'public/lib/morris.js', expand: true
                    },
                    {
                        cwd: 'bower_components/angular-morris-chart', src: [ '**/*.js' ],
                        dest: 'public/lib/angular-morris-chart', expand: true
                    },
                    {
                        cwd: 'bower_components/ionrangeslider', src: [ '**/*.js', '**/*.css' ],
                        dest: 'public/lib/ionrangeslider', expand: true
                    },
                    {
                        cwd: 'bower_components/angular-bootstrap', src: [ '**/*.js' ],
                        dest: 'public/lib/angular-bootstrap', expand: true
                    },
                    {
                        cwd: 'bower_components/angular-animate', src: [ '**/*.js' ],
                        dest: 'public/lib/angular-animate', expand: true
                    },
                    { cwd: 'bower_components/rangy', src: [ '**/*.js' ], dest: 'public/lib/rangy', expand: true },
                    {
                        cwd: 'bower_components/textAngular', src: [ '**/*.js', '**/*.css' ],
                        dest: 'public/lib/textAngular', expand: true
                    },
                    {
                        cwd: 'bower_components/angular-xeditable', src: [ '**/*.js', '**/*.css' ],
                        dest: 'public/lib/angular-xeditable', expand: true
                    },
                    {
                        cwd: 'bower_components/jstree', src: [ '**/*.js', '**/*.css' ],
                        dest: 'public/lib/jstree', expand: true
                    },
                    {
                        cwd: 'bower_components/ng-js-tree',
                        src: [ '**/*.js' ],
                        dest: 'public/lib/ng-js-tree',
                        expand: true
                    },
                    {
                        cwd: 'bower_components/angular-ui-select', src: [ '**/*.js', '**/*.css' ],
                        dest: 'public/lib/angular-ui-select', expand: true
                    },
                    {
                        cwd: 'bower_components/animate.css', src: [ '*.css' ],
                        dest: 'public/lib/animate.css', expand: true
                    },
                    {
                        cwd: 'bower_components/amcharts',
                        src: [ 'dist/amcharts/*.js', 'dist/amcharts/plugins/responsive/*.js', 'dist/amcharts/*.map', 'dist/amcharts/plugins/responsive/*.map' ],
                        dest: 'public/lib/amcharts',
                        expand: true
                    },
                    {
                        cwd: 'bower_components/ammap', src: [ 'dist/ammap/*.js', 'dist/ammap/maps/**/*.js' ],
                        dest: 'public/lib/ammap', expand: true
                    },
                    {
                        cwd: 'bower_components/angular-local-storage', src: [ 'dist/*.min.js', 'dist/*.map' ],
                        dest: 'public/lib/angular-local-storage', expand: true
                    },
                    {
                        cwd: 'bower_components/angular-resource', src: [ '*.min.js' ],
                        dest: 'public/lib/angular-resource', expand: true
                    },
                    {
                        cwd: 'bower_components/angular-spinner/dist/', src: [ '*.min.js' ],
                        dest: 'public/lib/angular-spinner', expand: true
                    },
                    {
                        cwd: 'bower_components/angular-material/', src: [ '*.js', '*.css' ],
                        dest: 'public/lib/angular-material', expand: true
                    },
                    {
                        cwd: 'bower_components/angular-aria/', src: [ '*.js' ],
                        dest: 'public/lib/angular-aria', expand: true
                    },
                    {
                        cwd: 'bower_components/angular-google-maps/dist', src: [ '*.js' ],
                        dest: 'public/lib/angular-google-maps', expand: true
                    },
                    //{cwd: 'bower_components/', src: ['**/*.css'], dest: 'public/lib/', expand: true},
                    //{cwd: 'bower_components/', src: ['**/*.js'], dest: 'public/lib/', expand: true},


                ]
            }
        },
        nodemon: {
            dev: {
                script: 'app.js',
                options: {
                    args: [ '--color' ],
                    ignore: [ 'README.md', 'node_modules/**', '.DS_Store' ],
                    ext: 'js',
                    watch: [ 'app', 'config', 'app.js', 'gruntfile.js' ],
                    delay: 1000,
                    env: {
                        PORT: 3000
                    },
                    cwd: __dirname
                }
            }
        },
        concurrent: {
            tasks: [ 'nodemon', 'watch' ],
            options: {
                logConcurrentOutput: true
            }
        },
        mochaTest: {
            options: {
                reporter: 'spec'
            },
            src: [ 'test/mocha/**/*.js' ]
        },
        env: {
            test: {
                NODE_ENV: 'test'
            }
        },
        karma: {
            unit: {
                configFile: 'test/karma/karma.conf.js'
            }
        }
    });

    // Load NPM tasks
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-nodemon');
    grunt.loadNpmTasks('grunt-concurrent');
    grunt.loadNpmTasks('grunt-mocha-test');
    grunt.loadNpmTasks('grunt-karma');
    grunt.loadNpmTasks('grunt-env');
    grunt.loadNpmTasks('grunt-copy');

    // Making grunt default to force in order not to break the project.
    grunt.option('force', true);

    // Default task(s).
    grunt.registerTask('default', [ 'copy', 'jshint', 'concurrent' ]);

    // Test task.
    grunt.registerTask('test', [ 'env:test', 'mochaTest', 'karma:unit' ]);
};