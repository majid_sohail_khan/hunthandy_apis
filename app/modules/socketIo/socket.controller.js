/**
 * Created by Raza on 17/8/2017.
 */
'use strict'; // NOSONAR
const _ = require('lodash'),
    winston = require('../../../config/winston'),
    chalk = require('chalk'),
    jsonKeys = require('./socket_keys.json'),
    globalLib = require('../globals/global.library'),
    mongoose = require('mongoose'),
    globalOnlineUsers = mongoose.model('globalOnlineUsers'),
    jobs = '',
    contractorAccounts = mongoose.model('contractorAccounts');

let inArray = (myArray, myValue) => {
    var inArray = false;
    myArray.map(function (key) {
        if ( key.userId === myValue ) {
            inArray = true;
        }
    });
    return inArray;
};

let createRoom = (Secret, userId, socket) => {
    socket.join(Secret);
};

let addGlobalUserSocket = (userId, socket) => {
    if ( userId ) {
        return globalOnlineUsers.update({ userId: userId }, { $set: { socketId: socket.id } }, { upsert: true })
            .catch(function (err) {
                winston.error(chalk.red('There was error adding global online user'));
                winston.error(err);
            });
    } else {
        return null;
    }
};

let removeGlobalUserSocket = (socketId, userId) => {
    var socketCheck = global.io.sockets.connected[ socketId ];
    if ( !socketCheck ) {
        return globalOnlineUsers.update({ userId: userId }, { $set: { socketId: '' } }).then(function (user) {
            // winston.info(chalk.green('Removing socket id from mongodb for user id = ' + userId));
            return user;
        }).catch(function (err) {
            winston.error(chalk.red('There was error removing socket from mongodb'));
            winston.error(err);
        });
    } else {
        winston.info(chalk.green('socket Id is still connected'));
    }
};

let getGlobalUserSocket = (userId) => {
    if ( userId ) {
        return globalOnlineUsers.findOne({ userId: userId }).then((sockets) => {
            if ( sockets ) {
                return sockets;
            } else {
                return null;
            }
        });
    }
    else {
        return null;
    }
};

let emitToSocket = (key, socketId, userId, socketData) => {
    if ( io.sockets.connected[ socketId ] ) {
        winston.info('Emiting to User Socket: ' + socketId + ' &key=' + key);
        console.log(socketData);
        io.sockets.connected[ socketId ].emit(key, socketData);
        return;
    } else {
        winston.error(chalk.red('Disconnected User SocketId=' + socketId));
        return removeGlobalUserSocket(socketId, userId);
    }
};

let emitToRoom = (room, key, socketData) => {
    winston.info('Emiting to Room: ' + room + ' &key=' + key + ' & data=' + JSON.stringify(socketData));
    io.to(room).emit(key, socketData);
    return;
};

let broadcastToAll = (key, socketData) => {
    winston.info('Broadcast to all & key=' + key);
    io.sockets.emit(key, socketData);
    return;
};

let addUserToRooms = (userId, socket) => {
    jobs.find({ $or: [ { user: userId }, { driver: userId } ] }).then(jobsFound => {
        if ( jobsFound.length > 0 ) {
            jobsFound.forEach(function (job) {
                if ( job.status === 'Accepted' || job.status === 'In-progress' || job.status === 'Delivered' ) {
                    socket.join(job._id);
                }
            });
        }
    });
};

let checkIfSocketConnected = (socketId) => {
    return global.io.sockets.connected[ socketId ];
};

let addUserToSingleRoom = (userId, room) => {
    winston.info('Adding user: ' + userId + ' to room : ' + room);
    getGlobalUserSocket(userId).then(socket => {
        if ( socket ) {
            io.sockets.connected[ socket.socketId ].join(room);
        }
    });
};

let updateMoverLocation = (userId, data, cb) => {
    let $filter = { _id: userId };
    let $update = {
        $set: {
            currentLocation: {
                type: 'Point',
                coordinates: _.reverse(data.latLong.split(',')),
            }
        }
    };

    contractorAccounts.findOneAndUpdate($filter, $update, { new: true }, (err, updatedMover) => {
        if ( err ) {
            return cb(err);
        } else {
            cb(null, updatedMover);
        }
    });
};

module.exports = {
    createRoom,
    addGlobalUserSocket,
    removeGlobalUserSocket,
    getGlobalUserSocket,
    emitToSocket,
    emitToRoom,
    broadcastToAll,
    addUserToRooms,
    checkIfSocketConnected,
    addUserToSingleRoom,
    updateMoverLocation,
};
