'use strict'; //NOSONAR

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;


var globalOnlineUsers = new Schema({
    userId: {type: String, index: {unique: true}},
    socketId: String
});

mongoose.model('globalOnlineUsers', globalOnlineUsers);
