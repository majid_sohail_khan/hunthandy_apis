/**
 * Created by Waqar on 1/8/2018.
 */

const mailer = require('../../../config/mailer'),
    randomize = require('randomatic'),
    _ = require('lodash'),
    moment = require('moment'),
    // winston = require('winston'),
    requestIp = require('request-ip'),
    mongoose = require('mongoose'),
    userAccounts = mongoose.model('userAccounts'),
    contractorAccounts = mongoose.model('contractorAccounts'),
    jobs = '',
    settings = mongoose.model('settings'),
    // userHelper = require('../userAccountss/controller/helpers/user_accounts.helper'),
    // moverHelper = require('../contractorAccountss/controller/helpers/mover_accounts.helper'),
    socketHelper = require('../socketIo/socket.controller'),
    agendaHelper = require('../agenda/agenda.helper'),
    logController = '';

let sendEmail = (email, body, vars, template, directory) => {
    return new Promise((resolve, reject) => {
        mailer.sendEmailer(email, body, vars, template, directory, (err) => {
            if ( err ) {
                return reject(err);
            } else {
                return resolve();
            }
        });
    });
};

let capitalizeFirstLetter = (string) => {
    return string.charAt(0).toUpperCase() + string.slice(1);
};

let generateCustomCode = (codeLength, expiryMinutes) => {
    const expTime = new Date();

    return { code: randomize('0', codeLength), expiryTime: addDays(expTime, expiryMinutes) };
};

let addDays = (date, days) => {
    let result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
};

let getUTCDateTime = dateTime => {
    return moment.utc(dateTime, 'YYYY-M-D');
};

let formatDate = dateTime => {
    return moment.utc(dateTime).format('YYYY-MM-DD');
};

let formatDateTime = dateTime => {
    return moment(dateTime).format('YYYY-M-D, hh:mm a');
};

let formatTime = dateTime => {
    if ( dateTime ) {
        return moment(dateTime).format('hh:mm a');
    } else {
        return;
    }
};

let isMobileNumberValid = (value) => {
    var regex = /^(?:(?:\+?1\s*(?:[.-]\s*)?)?(?:\(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\s*\)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?$/;

    if ( regex.test(value) ) {
        return true;
    } else {
        return false;
    }
};

let queryUserAndMover = (filter, cb) => {
    return userAccounts.findOne(filter).then(userFound => {
        if ( userFound ) {
            return userFound;
        }

        return contractorAccounts.findOne(filter).then(moverFound => {
            if ( moverFound ) {
                return moverFound;
            }
            else {
                return null;
            }
        });
    });
};

let enForceSingleSession = (req, res, next) => {
    console.log('======================= Single Session Function Triggred ==================================');
    return queryUserAndMover({ _id: req.acct._id }).then(acct => {
        if ( !acct ) {
            return next({ msgCode: 6000 });
        }
        if ( acct.sessionId !== req.sessionID ) {
            try {
                delete req.session[ acct.sessionId ];
                req.sessionStore.destroy(acct.sessionId, err => {
                    if ( err ) {
                        return next({ msgCode: 6002 });
                    }

                    if ( req.acct.userType === 'user' ) {
                        userAccounts.findOneAndUpdate({ _id: req.acct._id }, {
                            $set: { sessionId: req.sessionID }
                        }, { new: true }, (err, updatedMover) => {
                            if ( err ) {
                                return next({ msgCode: 6001 });
                            } else {
                                next();
                            }
                        });
                    } else {
                        contractorAccounts.findOneAndUpdate({ _id: req.acct._id }, {
                            $set: { sessionId: req.sessionID }
                        }, { new: true }, (err, updatedMover) => {
                            if ( err ) {
                                return next({ msgCode: 6001 });
                            } else {
                                next();
                            }
                        });
                    }

                });
            } catch ( err ) {
                next({ msgCode: 6002 });
            }
        } else {
            next();
        }
    });
};

let sendSocketDataAndNotification = (socketKey, socketRoom, userId, userType, notificationMessage, notificationType, notificationData, resource, callback) => {
    let pushType = 1;
    if ( notificationType === 'jobChat' ) {
        pushType = 2;
    }
    let socket = { resource };
    socketHelper.getGlobalUserSocket(userId).then(socketConnected => {
        if ( socketRoom ) {
            socketHelper.emitToRoom(socketRoom, socketKey, { socket });
            callback(null);
        } else if ( socketConnected ) {
            if ( socketConnected.socketId !== '' && socketHelper.checkIfSocketConnected(socketConnected.socketId) ) {
                socketHelper.emitToSocket(socketKey, socketConnected.socketId, userId, { socket });
                return callback(null);
            } else {
                agendaHelper.pushJob({
                    userId: userId,
                    message: notificationMessage,
                    messageType: notificationType,
                    resource: notificationData,
                    statusCase: socketKey,
                    userType: userType
                }, pushType);
                callback(null);
            }
        } else {
            agendaHelper.pushJob({
                userId: userId,
                message: notificationMessage,
                messageType: notificationType,
                resource: notificationData,
                statusCase: socketKey,
                userType: userType
            }, pushType);
            callback(null);
        }
    });
};

let invertArray = array => {
    return array.reverse();
};

let convertArrayToString = array => {
    // return array.toString();
    return array.join(', ').toString();
};

let sortArray = array => {
    return array.sort();
};

let addCompanyPercentage = (actualAmount, settings) => {
    if ( settings ) {
        return (actualAmount + (actualAmount * (settings.userCompanyPercentage / 100)));
    } else {
        return (actualAmount + (actualAmount * (config.companyPercentage / 100)));
    }
};

let fetchIPAdress = (req, res, next) => {
    const clientIp = requestIp.getClientIp(req);
    req.clientIp = clientIp;
    next();
};

let updateUser = (filter, update, cb) => {
    userAccounts.findOneAndUpdate(filter, update, { new: true }, (err, user) => {
        if ( err ) {
            return cb(err);
        } else {
            cb(null, user);
        }
    });
};

let isJobOpen = (jobId) => {
    return jobs.findOne({ _id: jobId }).then(jobFound => {
        if ( jobFound ) {
            if ( jobFound.status === 'Open' ) {
                return jobFound;
            } else {
                return null;
            }
        } else {
            return null;
        }
    });
};

let diffTime = (endTime, startTime) => {
    if ( startTime && endTime ) {
        let jobDuration = moment.utc(endTime, 'milliseconds').diff(startTime);
        return Math.ceil(moment.duration(jobDuration, 'milliseconds').format('hh.mm'));
    } else {
        return;
    }
};

let updateMover = (filter, update, cb) => {
    contractorAccounts.findOneAndUpdate(filter, update, { new: true }, (err, updatedMover) => {
        if ( err ) {
            return cb(err);
        } else {
            cb(null, updatedMover);
        }
    });
};

let fetchIPAddress = (req, res, next) => {
    const clientIp = requestIp.getClientIp(req);
    req.clientIp = clientIp;
    next();
};

let isJobExists = (req, res, next) => {
    const jobId = (req.body.jobId) ? _.trim(req.body.jobId) : _.trim(req.params.jobId);
    return jobs.findOne({ _id: jobId }).then(jobFound => {
        if ( jobFound ) {
            req.jobDetail = jobFound;
            return next();
        } else {
            return null;
        }
    });
};

let removeJobSlot = (jobId, moverId, cb) => {
    return contractorAccounts.findOneAndUpdate({ _id: moverId }, {
        $pull: { 'jobSlots': { jobId: jobId } }
    }, { new: true }).then((updatedMover) => {
        if ( updatedMover ) {
            return cb();
        } else {
            return cb();
        }
    }).catch((err) => {
        return cb(err);
    });
};

let getSystemSettings = (req, res, next) => {
    return settings.findOne({}).then(systemSettings => {
        if ( systemSettings ) {
            req.settings = systemSettings;
            return next();
        } else {
            return next({ msgCode: 5065 });
        }
    }).catch(err => {
        logController.createLog('error', 'high', 'User tried to get system settings but failed.', req.user.userType, req.user._id, { err: (err.TypeError) ? err.TypeError : err }, req.clientIp);
        return next({ msgCode: 5065 });
    });
};

let convertSecondsIntoTimeFormat = (seconds) => {
    if ( seconds < 60 ) {
        seconds = 60;
    }
    let startTime = moment().startOf('day');
    let endTime = moment().startOf('day').seconds(seconds);
    let duration = moment.duration(endTime.diff(startTime));
    let hours = parseInt(duration.asHours());
    let minutes = parseInt(duration.asMinutes()) % 60;

    let formattedTime = '';
    if ( hours > 0 ) {
        if ( minutes > 0 ) {
            formattedTime = hours + ' hr(s) and ' + minutes + ' min(s)';
        } else {
            formattedTime = hours + ' hr(s)';
        }

    } else {
        formattedTime = minutes + ' min(s)';
    }
    return formattedTime;
};

let convertMinutesIntoTimeFormat = (mintsProvided) => {
    if ( mintsProvided < 60 ) {
        mintsProvided = 60;
    }

    let startTime = moment().startOf('day');
    let endTime = moment().startOf('day').minutes(mintsProvided);
    let duration = moment.duration(endTime.diff(startTime));
    let hours = parseInt(duration.asHours());
    let minutes = parseInt(duration.asMinutes()) % 60;

    let formattedTime = '';
    if ( hours > 0 ) {
        if ( minutes > 0 ) {
            formattedTime = hours + ' hr(s) and ' + minutes + ' min(s)';
        } else {
            formattedTime = hours + ' hr(s)';
        }

    } else {
        formattedTime = minutes + ' min(s)';
    }
    return formattedTime;
};

module.exports = {
    sendEmail,
    sendSocketDataAndNotification,
    generateCustomCode,
    isMobileNumberValid,
    getUTCDateTime,
    enForceSingleSession,
    formatDate,
    formatDateTime,
    capitalizeFirstLetter,
    invertArray,
    convertArrayToString,
    sortArray,
    updateUser,
    addCompanyPercentage,
    fetchIPAdress,
    queryUserAndMover,
    isJobOpen,
    diffTime,
    formatTime,
    isJobExists,
    updateMover,
    fetchIPAddress,
    removeJobSlot,
    getSystemSettings,
    convertSecondsIntoTimeFormat,
    convertMinutesIntoTimeFormat
};