/**
 * Created by Raza on 15/01/2018.
 */

const winston = require('winston'),
    ucfirst = require('ucfirst'),
    responseModule = require('../../../../config/response');


let validationResponse = (message, req, next) => {
    const errors = req.validationErrors();

    if ( errors ) {
        winston.error(message, errors[ 0 ]);
        return next(errors[ 0 ]);
    }

    return next();
};

let validateBasicSignUp = (req, res, next) => {
    req.body.email = req.body.email.toLowerCase();
    req.body.firstName = ucfirst(req.body.firstName);
    req.body.lastName = ucfirst(req.body.lastName);
    req.assert('firstName', 2003).notEmpty();
    req.assert('lastName', 2004).notEmpty();
    req.assert('email', 2001).notEmpty();
    req.assert('email', 2012).isEmail();
    req.assert('phoneNumber', 2002).notEmpty();
    req.assert('phoneNumber', 2013).isPhoneNumberValid();
    // req.assert('gender', 2005).notEmpty();
    // req.assert('gender', 2021).isValidGenderType();
    req.assert('dob', 2006).notEmpty();
    req.assert('address', 2007).notEmpty();
    req.assert('state', 2008).notEmpty();
    req.assert('city', 2009).notEmpty();
    req.assert('postalCode', 2010).notEmpty();
    // req.assert('ssn', 2011).notEmpty();
    // req.assert('ssn', 2022).isValidSocialSecurityNumber();
    req.assert('deviceType', 2019).notEmpty();
    req.assert('deviceToken', 2020).notEmpty();

    return validationResponse('Mover could not be sign up: ', req, next);
};

let validateSignUpStepTwo = (req, res, next) => {
    req.assert('servicesOffered', 2029).notEmpty();
    req.assert('hourlyRate', 2030).notEmpty();
    req.assert('bio', 2031).notEmpty();

    if ( req.body.hourlyRate < config.minHourlyRate || req.body.hourlyRate > config.maxHourlyRate ) {
        return responseModule.successResponse(res, {
            success: 0,
            message: 'Hourly rate limit is $' + config.minHourlyRate + ' - $' + config.maxHourlyRate,
            data: {}
        });
    }

    validationResponse('mover could not perform sign up step two action.', req, next);
};

let confirmAccountValidate = (req, res, next) => {
    req.assert('phoneNumber', 2002).notEmpty();
    req.assert('confirmationCode', 2023).notEmpty();
    req.assert('deviceType', 5008).notEmpty();
    req.assert('deviceType', 5008).isValidDeviceType();
    req.assert('deviceToken', 2020).notEmpty();

    validationResponse('mover could not perform confirm account action.', req, next);
};

let resendCodeValidate = (req, res, next) => {
    req.assert('phoneNumber', 2002).notEmpty();

    validationResponse('mover could not perform resend code action.', req, next);
};

let logInValidate = (req, res, next) => {
    req.assert('phoneNumber', 2002).notEmpty();
    req.assert('loginCode', 2023).notEmpty();

    validationResponse('mover could not be logged in', req, next);
};

let validateMoverOnline = (req, res, next) => {
    req.assert('online', 2033).isBoolean();

    validationResponse('mover could not be toggle his online status', req, next);
};

let moverProfileValidation = (req, res, next) => {
    req.body.firstName = ucfirst(req.body.firstName);
    req.body.lastName = ucfirst(req.body.lastName);
    req.assert('firstName', 2003).notEmpty();
    req.assert('lastName', 2004).notEmpty();
    // req.assert('gender', 2005).notEmpty();
    // req.assert('gender', 2021).isValidGenderType();
    req.assert('dateOfBirth', 2006).notEmpty();
    req.assert('address', 2007).notEmpty();
    req.assert('state', 2008).notEmpty();
    req.assert('city', 2009).notEmpty();
    req.assert('postalCode', 2010).notEmpty();
    // req.assert('ssn', 2011).notEmpty();
    // req.assert('ssn', 2022).isValidSocialSecurityNumber();

    return validationResponse('Mover could not be edit his profile: ', req, next);
};

module.exports = {
    validateBasicSignUp,
    validateSignUpStepTwo,
    confirmAccountValidate,
    resendCodeValidate,
    logInValidate,
    validateMoverOnline,
    moverProfileValidation
};