/**
 * Created by Waqar on 18/01/2018.
 */
const _ = require('lodash'),
    NodeGeocoder = require('node-geocoder'),
    async = require('async'),
    passport = require('passport'),
    mongoose = require('mongoose'),
    moment = require('moment'),
    winston = require('winston'),

    tempUserAccount = mongoose.model('tempUserAccount'),
    contractorAccounts = mongoose.model('contractorAccounts'),
    Weeks = '',
    Jobs = '',
    spWeeklyEarnings = '',
    contents = '',
    feedbacks = '',

    responseModule = require('../../../../config/response'),
    sms = require('../../../../config/sms'),
    multer = require('../../../../config/multer'),
    pushNotification = require('../../../../config/notifications'),

    commonLib = require('../../globals/global.library'),
    logController = '',
    moverHelper = require('./helpers/mover_accounts.helper'),
    userHelper = require('../../userAccounts/controller/helpers/user_accounts.helper'),

    options = {
        provider: 'google',
        // Optional depending on the providers
        httpAdapter: 'https', // Default
        apiKey: config.googleMapsAPI, // for Mapquest, OpenCage, Google Premier
        formatter: null         // 'gpx', 'string', ...
    },
    geocoder = NodeGeocoder(options);

let isAccountExists = (req, res, next) => {
    const email = _.trim(req.body.email);
    const phoneNumber = _.trim(req.body.phoneNumber);
    let $filter = {
        $or: [ { email: email }, { phoneNumber: phoneNumber } ]
    };

    contractorAccounts.findOne($filter).then(iscontractorAccounts => {
        if ( iscontractorAccounts ) {
            return next({ msgCode: 2014 });
        } else {
            return next();
        }
    }).catch(err => {
        return next({ msgCode: 2015 });
    });
};

let basicSignUp = (req, res, next) => {
    const firstName = _.trim(req.body.firstName),
        lastName = _.trim(req.body.lastName),
        profileImage = _.trim(req.body.profileImage),
        address = _.trim(req.body.address),
        email = _.trim(req.body.email),
        phoneNumber = _.trim(req.body.phoneNumber),
        dob = _.trim(commonLib.getUTCDateTime(req.body.dob)),
        deviceType = _.trim(req.body.deviceType),
        deviceToken = _.trim(req.body.deviceToken),
        city = _.trim(req.body.city),
        state = _.trim(req.body.state),
        postalCode = _.trim(req.body.postalCode),
        socialSecurityNumber = _.trim(req.body.ssn);

    return geocoder.geocode(address + ' ' + city + ' ' + state + ' US').then(geoResponse => {
    	console.log(geoResponse)

        if ( !geoResponse.length ) {
            return next({ msgCode: 5031 });
        }

        if ( geoResponse.length > 0 ) {
        	if(geoResponse[0].countryCode !== 'US'){
                throw { msgCode: 2039 };
        	}

            if ( geoResponse[ 0 ].zipcode ) {
                if ( geoResponse[ 0 ].zipcode !== postalCode ) {
                    throw { msgCode: 2017 };
                }
            }
        }

        let codeObject = userHelper.generateVerificationCode(config.confirmationCodeLength, config.confirmAcctCodeExpiryMinutes);
        let tempMoverCreateObject = {
            confirmationCode: codeObject.code,
            confirmationCodeExpirationTime: codeObject.expiryTime,
            firstName: firstName,
            lastName: lastName,
            name: firstName + ' ' + lastName,
            profileImage: profileImage,
            deviceType: deviceType,
            deviceToken: deviceToken,
            phoneNumber: phoneNumber,
            email: email,
            dateOfBirth: dob,
            address: address,
            city: city,
            state: state,
            postalCode: postalCode,
            userType: 'mover',
            socialSecurityNumber: socialSecurityNumber
        };

        if ( !profileImage ) {
            delete tempMoverCreateObject.profileImage;
        }

        if ( req.body.gender ) {
            tempMoverCreateObject.gender = _.trim(req.body.gender);
        }

        return tempUserAccount.findOneAndUpdate({ email: tempMoverCreateObject.email }, { $set: tempMoverCreateObject }, {
            upsert: true,
            new: true
        }).then(tempMoverCreated => {
            async.parallel([
                SMSCb => {
                    let smsObject = {
                        to: phoneNumber,
                        text: 'MoveBig: ' + 'Verification code' + ' is ' + tempMoverCreated.confirmationCode,
                    };
                    sms.sendMessage(smsObject, (err) => {
                        if ( err ) {
                            return SMSCb('Error: Verification Code SMS not sent due to technical reasons. Please try later.');
                        }
                        else {
                            return SMSCb();
                        }
                    });
                },
                mailCb => {
                    let vars = {
                            verificationCode: tempMoverCreated.confirmationCode,
                            name: tempMoverCreated.name
                        },
                        subject = 'MoveBig: Account verification';
                    commonLib.sendEmail(tempMoverCreated.email, subject, vars, 'account_verification', 'userAccounts').then(() => {
                        mailCb();
                    }).catch(err => {
                        mailCb('Error: Verification Code email not sent due to technical reasons. Please try later.');
                    });
                }
            ], err => {
                if ( err ) {
                    return next({ msgCode: 5037 });
                }
                else {
                    logController.createLog('info', 'normal', tempMoverCreated.name + ' has signed up successfully', tempMoverCreated.userType, tempMoverCreated._id, {}, req.clientIp);
                    return responseModule.successResponse(res, {
                        success: 1,
                        message: 'Verification code has been sent to your email and phone.',
                        data: { code: tempMoverCreated.confirmationCode }
                    });
                }
            });
        });
    }).catch(err => {
        logController.createLog('error', 'high', firstName + ' ' + lastName + ' tried to sign up but failed.', 'mover', null, {}, req.clientIp);
        return next({ msgCode: err.msgCode || 2016 });
    });
};

let signUpStepTwo = (req, res, next) => {
    const hourlyRate = req.body.hourlyRate,
        servicesOffered = req.body.servicesOffered,
        bio = req.body.bio;

    let filter = { _id: req.user._id },
        updatedObj = { hourlyRate: hourlyRate, servicesOffered: servicesOffered, bio: bio, stepCompleted: 2 };

    moverHelper.updateMover(filter, updatedObj).then(updatedMover => {
        logController.createLog('info', 'normal', req.user.firstName + ' ' + req.user.lastName + ' has completed sign up step two successfully', req.user.userType, req.user._id, {}, req.clientIp);

        return responseModule.successResponse(res, {
            success: 1,
            message: 'Mover Sign up process is completed.',
            data: updatedMover ? userHelper.generateUserResponse(updatedMover) : {}
        });
    }).catch(err => {
        logController.createLog('error', 'high', req.user.firstName + ' ' + req.user.lastName + ' tried to do sign up step two but failed.', req.user.userType, req.user._id, {}, req.clientIp);
        return next({ msgCode: 2032 });
    });
};

let confirmAccount = (req, res, next) => {
    let phoneNumber = _.trim(req.body.phoneNumber);
    let filter = { phoneNumber: _.trim(phoneNumber), userType: 'mover' };
    return moverHelper.queryMover(filter).then(moverFound => {
        if ( moverFound ) {
            return next({ msgCode: 2024 });
        }
        return userHelper.queryTempUser(filter).then(tempUserFound => {
            if ( !tempUserFound ) {
                return next({ msgCode: 2026 });
            }

            if ( parseInt(tempUserFound.confirmationCode) !== parseInt(req.body.confirmationCode) ) {
                return next({ msgCode: 5013 });
            }

            if ( tempUserFound.confirmationCodeExpirationTime <= new Date() ) {
                return next({ msgCode: 5014 });
            }
            let address = {
                streetAddress: tempUserFound.address,
                city: tempUserFound.city,
                state: tempUserFound.state,
                postalCode: tempUserFound.postalCode
            };

            let moverObj = {
                firstName: tempUserFound.firstName,
                lastName: tempUserFound.lastName,
                name: tempUserFound.name,
                gender: tempUserFound.gender,
                dateOfBirth: tempUserFound.dateOfBirth,
                profileImage: tempUserFound.profileImage || '',
                deviceType: tempUserFound.deviceType,
                deviceToken: tempUserFound.deviceToken,
                phoneNumber: tempUserFound.phoneNumber,
                email: tempUserFound.email,
                isPhoneVerified: true,
                userType: 'mover',
                address: address,
                socialSecurityNumber: tempUserFound.socialSecurityNumber,
                stepCompleted: 1,
                isOnline: true,
                currentLocation: { type: 'Point', coordinates: [ 0, 0 ] },
                isAgreeToTermsAndCondition: true,
                isAgreeToDisclosure: true,
                isReceiveBackGroundCheckReport: true,
                agreeUnixTimeStamp: moment().unix(),
            };
            new contractorAccounts(moverObj).save().then(moverCreated => {
                if ( moverCreated ) {
                    tempUserAccount.findByIdAndRemove({ _id: tempUserFound._id }).then(deleteTempUser => {
                        req.login(moverCreated, err => {
                            if ( err ) {
                                return next({ msgCode: 2027 });
                            }
                            if ( req.body.deviceType && req.body.deviceToken ) {
                                pushNotification.generateEndPoint(req).then(endPoint => {
                                    if ( endPoint ) {
                                        let filter = { _id: req.user._id },
                                            update = {
                                                deviceType: req.body.deviceType,
                                                deviceToken: req.body.deviceToken,
                                                snsToken: endPoint
                                            };

                                        return contractorAccounts.findOneAndUpdate(filter, { $set: update }, { new: true }).then(moverFound => {
                                            if ( !moverFound ) {
                                                return next({ msgCode: 5055 });
                                            }
                                        });
                                    } else {
                                        return next({ msgCode: 5056 });
                                    }
                                });
                            } else {
                                return next({ msgCode: 5057 });
                            }
                            let moverObj = userHelper.generateUserResponse(moverCreated);
                            moverObj.dateOfBirth = commonLib.formatDate(moverObj.dateOfBirth);

                            logController.createLog('info', 'normal', req.user.firstName + ' ' + req.user.lastName + ' verified his account successfully', req.user.userType, req.user._id, {}, req.clientIp);

                            let vars = { text: 'A new mover "' + moverCreated.name + '" has been signed up.' },
                                subject = 'MoveBig: Mover SignUp';

                            commonLib.sendEmail(moverCreated.email, 'Welcome to MobiMover!', {}, 'welcome', 'contractorAccountss')
                            .catch(err => {
                                winston.error(err);
                            });  

                            commonLib.sendEmail('brett.polymove@gmail.com', subject, vars, 'general', 'userAccounts')
                                .catch(err => {
                                    winston.error(err);
                                });

                            return responseModule.successResponse(res, {
                                success: 1,
                                message: 'Mover verified successfully.',
                                data: moverObj
                            });
                        });
                    });
                } else {
                    return next({ msgCode: 2028 });
                }
            });
        });
    }).catch(err => {
        logController.createLog('error', 'high', req.user.firstName + ' ' + req.user.lastName + ' tried to verify his account but failed.', req.user.userType, req.user._id, {}, req.clientIp);
        return next({ msgCode: 5039 });
    });
};

let sendLoginCode = (req, res, next) => {
    let codeGeneration = userHelper.generateVerificationCode(config.confirmationCodeLength, config.confirmAcctCodeExpiryMinutes);

    let userPhoneNumber = _.trim(req.body.phoneNumber),
        confirmationCode = codeGeneration.code,
        confirmationCodeExpirationTime = codeGeneration.expiryTime;

    let updateObject = {
        loginCode: confirmationCode,
        loginCodeExpirationTime: confirmationCodeExpirationTime
    };

    return contractorAccounts.findOneAndUpdate({ phoneNumber: userPhoneNumber }, { $set: updateObject }, { new: true }).then(moverUpdated => {
        if ( moverUpdated ) {
            if ( moverUpdated.isBlocked ) {
                return next({ msgCode: '0007' });
            } else if ( moverUpdated.adminRejected ) {
                return next({ msgCode: '0006' });
            } else {
                async.series([
                    (SMSCb) => {
                        let smsObject = {
                            to: moverUpdated.phoneNumber,
                            text: 'MoveBig: ' + 'Login code' + ' is ' + confirmationCode,
                        };

                        sms.sendMessage(smsObject, (err) => {
                            if ( err ) {
                                return SMSCb('Verification Code SMS not sent due to technical reasons. Please try later. ');
                            } else {
                                return SMSCb();
                            }
                        });
                    },
                    (mailCb) => {
                        let vars = {
                                confirmationCode: confirmationCode,
                                name: moverUpdated.name
                            },
                            subject = 'MoveBig: Login Code';
                        commonLib.sendEmail(moverUpdated.email, subject, vars, 'login', 'userAccounts').then(() => {
                            mailCb();
                        }).catch(err => {
                            mailCb('There was some error in sending email.');
                        });
                    }
                ], (err) => {
                    if ( err ) {
                        return next({ msgCode: 5037 });
                    } else {
                        logController.createLog('info', 'normal', moverUpdated.firstName + ' ' + moverUpdated.lastName + ' sent login code successfully', moverUpdated.userType, moverUpdated._id, {}, req.clientIp);
                        return responseModule.successResponse(res, {
                            success: 1,
                            message: 'Login code sent successfully.',
                            data: { code: confirmationCode }
                        });
                    }
                });
            }


        } else {
            updateObject = {
                confirmationCode: confirmationCode,
                confirmationCodeExpirationTime: confirmationCodeExpirationTime
            };

            return tempUserAccount.findOneAndUpdate({ phoneNumber: userPhoneNumber }, { $set: updateObject }, { new: true }).then(tempUserUpdated => {
                if ( tempUserUpdated ) {
                    async.series([
                        (SMSCb) => {
                            let smsObject = {
                                to: tempUserUpdated.phoneNumber,
                                text: 'MoveBig: ' + 'Verification code' + ' is ' + confirmationCode,
                            };

                            sms.sendMessage(smsObject, (err) => {
                                if ( err ) {
                                    return SMSCb('SMS not sent due to technical reasons. Please try later. ');
                                } else {
                                    return SMSCb();
                                }
                            });
                        },
                        (mailCb) => {
                            let vars = {
                                    verificationCode: confirmationCode,
                                    name: tempUserUpdated.name
                                },
                                subject = 'MoveBig: Account verification';
                            commonLib.sendEmail(tempUserUpdated.email, subject, vars, 'account_verification', 'userAccounts').then(() => {
                                mailCb();
                            }).catch(err => {
                                mailCb(err);
                            });
                        }
                    ], (err) => {
                        if ( err ) {
                            return next({ msgCode: 5037 });
                        } else {
                            logController.createLog('info', 'normal', tempUserUpdated.firstName + ' ' + tempUserUpdated.lastName + ' resent signup code successfully', tempUserUpdated.userType, tempUserUpdated._id, {}, req.clientIp);
                            return responseModule.successResponse(res, {
                                success: 1,
                                message: 'Verification code resent successfully.',
                                data: { code: confirmationCode }
                            });
                        }
                    });
                } else {
                    throw { msgCode: 5032 };
                }
            }).catch(err => {
                logController.createLog('error', 'high', userPhoneNumber + ' tried to send login code but failed.', 'mover', 'Unknown', {}, req.clientIp);
                return next({ msgCode: 5032 });
            });
        }
    });
};

let logInMover = (req, res, next) => {
    passport.authenticate('mover', (err, user, info) => {
        if ( err ) {
            return next({ msgCode: 5036 });
        }
        if ( !user ) {
            return next({ msgCode: 5045 });
        }
        req.logIn(user, (err) => {
            if ( err ) {
                return next({ msgCode: 5036 });
            }

            req.acct = user;
            next();
        });
    })(req, res, next);
};

let registerForPushNotification = (req, res, next) => {
    if ( config.deviceTypes.indexOf(req.body.deviceType) > -1 ) {
        if ( req.body.deviceType && req.body.deviceToken ) {
            pushNotification.generateEndPoint(req).then(endPoint => {
                if ( endPoint ) {
                    let filter = { _id: req.user._id },
                        update = {
                            deviceType: req.body.deviceType,
                            deviceToken: req.body.deviceToken,
                            snsToken: endPoint
                        };

                    return contractorAccounts.findOneAndUpdate(filter, { $set: update }, { new: true }).then(moverFound => {
                        if ( moverFound ) {
                            return next();
                        } else {
                            return next({ msgCode: 5055 });
                        }
                    });
                } else {
                    return next({ msgCode: 5056 });
                }
            });
        } else {
            return next({ msgCode: 5057 });
        }
    } else {
        return next({ msgCode: 5046 });
    }
};

let moverLoginSuccess = (req, res, next) => {
    const userId = _.trim(req.acct._id);
    let moverObj = userHelper.generateUserResponse(req.acct);
    moverObj.dateOfBirth = commonLib.formatDate(moverObj.dateOfBirth);

    contractorAccounts.findOneAndUpdate({ _id: userId }, { $set: { isOnline: true } }, { new: true }).exec();
    logController.createLog('info', 'normal', req.user.firstName + ' ' + req.user.lastName + ' has logged in successfully', req.user.userType, req.user._id, {}, req.clientIp);

    responseModule.successResponse(res, {
        success: 1,
        message: 'Mover logged in successfully.',
        data: moverObj ? moverObj : {}
    });
};

let mediaUploaded = (req, res, next) => {
    try {
        multer.resizeAndUpload(req.files[ 0 ].location, req.files[ 0 ].key.split('1000X1000/')[ 1 ]);

        logController.createLog('info', 'normal', 'Mover has uploaded image successfully', 'mover', '', {}, req.clientIp);
        return responseModule.successResponse(res, {
            success: 1,
            message: '',
            data: { url: req.files[ 0 ].location }
        });
    }
    catch ( err ) {
        logController.createLog('error', 'high', 'Mover tried to upload image but failed.', 'mover', '', {}, req.clientIp);
        return next({ msgCode: 5034 });
    }
};

let logOutAccount = (req, res, next) => {
    let sid = req.sessionID;
    let user = req.user;
    pushNotification.removeEndPointUsingSessionId(sid);

    req.session.destroy(err => {
        req.logout();
    });

    contractorAccounts.findOneAndUpdate({ _id: _.trim(user._id) }, {
        $set: {
            isOnline: false,
            deviceType: '',
            deviceToken: '',
            snsToken: ''
        }
    }, { new: true }).exec();

    logController.createLog('info', 'normal', user.firstName + ' ' + user.lastName + ' has logged out successfully', req.user.userType, req.user._id, {}, req.clientIp);
    responseModule.successResponse(res, {
        success: 1,
        message: 'Mover logged out successfully.',
        data: {}
    });
};

let getMoverStatus = (req, res, next) => {
    let filter = { _id: req.user._id };

    contractorAccounts.findOne(filter).then(mover => {
        if ( mover ) {
            logController.createLog('info', 'normal', req.user.firstName + ' ' + req.user.lastName + ' fetched his online status successfully', req.user.userType, req.user._id, {}, req.clientIp);
            return responseModule.successResponse(res, {
                success: 1,
                message: 'Mover online status fetched successfully.',
                data: { online: mover.isOnline }
            });
        } else {
            return next({ msgCode: 5051 });
        }
    });
};

let toggleMoverOnlineStatus = (req, res, next) => {
    let filter = { _id: req.user._id };
    let onlineStatus = {
        isOnline: req.body.online
    };

    return contractorAccounts.findOneAndUpdate(filter, { $set: onlineStatus }, { new: true }).then(mover => {
        if ( mover ) {
            let message = 'Mover status is set to offline successfuly';
            if ( mover.isOnline ) {
                message = 'Mover status is set to online successfuly';
            }

            logController.createLog('info', 'normal', req.user.firstName + ' ' + req.user.lastName + ' updated his online status successfully', req.user.userType, req.user._id, {}, req.clientIp);
            return responseModule.successResponse(res, {
                success: 1,
                message: message,
                data: { online: mover.isOnline }
            });
        } else {
            return next({ msgCode: 5052 });
        }
    });
};

let getCurrentMover = (req, res, next) => {
    let filter = { _id: req.user._id };
    return contractorAccounts.findOne(filter).then(moverFound => {
        let moverObj = userHelper.generateUserResponse(moverFound);
        moverObj.dateOfBirth = commonLib.formatDate(moverObj.dateOfBirth);
        return responseModule.successResponse(res, {
            success: 1,
            message: 'Mover profile fetched successfully.',
            data: moverObj
        });
    });
};

let updateMoverProfile = (req, res, next) => {
    let profileImage = req.body.profileImage,
        firstName = req.body.firstName,
        lastName = req.body.lastName,
        dateOfBirth = req.body.dateOfBirth,
        streetAddress = req.body.address,
        state = req.body.state,
        city = req.body.city,
        postalCode = req.body.postalCode,
        address = {
            streetAddress: streetAddress,
            city: city,
            state: state,
            postalCode: postalCode,
        },
        name = firstName + ' ' + lastName,
        bio = req.body.bio,
        hourlyRate = req.body.hourlyRate,
        servicesOffered = req.body.servicesOffered,

        filter = { _id: req.user._id };

    if ( servicesOffered.length ) {
        servicesOffered.forEach(service => {
            if ( config.servicesOffered.indexOf(service) == -1 ) {
                return next({ msgCode: 5053 });
            }
        });
    }

    return geocoder.geocode(streetAddress + ' ' + city + ' ' + state + ' US').then(geoResponse => {
        if ( !geoResponse.length ) {
            return next({ msgCode: 5031 });
        }

        if ( geoResponse.length > 0 ) {
            if ( geoResponse[ 0 ].zipcode ) {
                if ( geoResponse[ 0 ].zipcode != req.body.postalCode ) {
                    return next({ msgCode: 5031 });
                }
            }
        }

        let updateObj = {
            firstName: firstName,
            lastName: lastName,
            name: name,
            profileImage: profileImage,
            dateOfBirth: dateOfBirth,
            address: address,
            bio: bio,
            hourlyRate: hourlyRate,
            servicesOffered: servicesOffered
        };

        if ( !profileImage ) {
            delete updateObj.profileImage;
        }

        if ( req.body.gender ) {
            updateObj.gender = _.trim(req.body.gender);
        }

        return contractorAccounts.findOneAndUpdate(filter, { $set: updateObj }, { new: true }).then(updatedMover => {
            if ( updatedMover ) {
                logController.createLog('info', 'normal', req.user.firstName + ' ' + req.user.lastName + ' updated his profile successfully', req.user.userType, req.user._id, {}, req.clientIp);

                let moverObj = userHelper.generateUserResponse(updatedMover);
                moverObj.dateOfBirth = commonLib.formatDate(moverObj.dateOfBirth);

                return responseModule.successResponse(res, {
                    success: 1,
                    message: 'Your profile has been updated successfully.',
                    data: moverObj
                });
            } else {
                logController.createLog('error', 'high', req.user.firstName + ' ' + req.user.lastName + ' tried to update his profile but failed.', req.user.userType, req.user._id, {}, req.clientIp);
                return next({ msgCode: 5049 });
            }
        }).catch(err => {
            logController.createLog('error', 'high', req.user.firstName + ' ' + req.user.lastName + ' tried to update his profile but failed.', req.user.userType, req.user._id, {}, req.clientIp);
            return next({ msgCode: 5049 });
        });
    });
};

let getEarningWeeks = (req, res, next) => {
    let offset = parseInt(req.query.offset ? req.query.offset : 0, 10);
    let limit = parseInt(req.query.limit ? req.query.limit : 20, 10);
    let joiningDate = moment(req.user.createdAt);
    let weekNumber = joiningDate.isoWeek();
    let weekYear = joiningDate.year();
    let filter = { weekNumber: { $gte: weekNumber }, weekYear: { $gte: weekYear } };
    let fields = { title: 1, weekNumber: 1, weekYear: 1 };
    Weeks.find(filter, fields).sort({ 'weekNumber': -1, 'weekYear': -1 }).skip(offset).limit(limit).then(weeks => {
        if ( weeks ) {
            return responseModule.successResponse(res, {
                success: 1,
                message: 'Mover earning weeks fetched successfully.',
                data: { weeks: weeks }
            });
        } else {
            return responseModule.successResponse(res, {
                success: 1,
                message: 'Mover earning weeks fetched successfully.',
                data: { weeks: [] }
            });
        }

    }).catch(err => {
        logController.createLog('error', 'high', err, req.user.userType, req.user._id, {}, req.clientIp);
        return next({ msgCode: 2034 });
    });
};

let getDetailWeeklyEarningReport = (req, res, next) => {
    let moverId = req.user._id,
        weekNumber = moment().isoWeek(),
        year = moment().year();

    if ( req.query.weekNumber && req.query.weekYear ) {
        weekNumber = parseInt(req.query.weekNumber),
            year = parseInt(req.query.weekYear);
    }

    let $filter = { moverId: moverId, weekNumber: weekNumber, year: year };
    let $fields = { weekNumber: 1, year: 1, totalJobs: 1, weeklyEarning: 1, jobs: 1 };

    spWeeklyEarnings.findOne($filter, $fields).lean().then(weeklyEarnings => {

        let weekStart = moment().year(year).week(weekNumber).startOf('isoweek').isoWeekday(1);
        let weekEnd = moment().year(year).week(weekNumber).startOf('isoweek').isoWeekday(7);
        let weekPeriod = '';

        if ( weekStart.month() === weekEnd.month() ) {
            weekPeriod = weekStart.date() + '-' + weekEnd.date() + ' ' + weekEnd.format('MMMM');
        } else {
            weekPeriod = weekStart.date() + ' ' + weekStart.format('MMMM') + '-' + weekEnd.date() + ' ' + weekEnd.format('MMMM');
        }
        if ( weeklyEarnings ) {
            weeklyEarnings.weekTitle = weekPeriod;
            weeklyEarnings.weeklyEarning = parseFloat(weeklyEarnings.weeklyEarning.toFixed(2));
            return responseModule.successResponse(res, {
                success: 1,
                message: 'Mover earning weeks fetched successfully.',
                data: { weeklyEarning: weeklyEarnings }
            });
        } else {
            let weekEmpty = {
                weekTitle: weekPeriod,
                weekNumber: weekNumber,
                year: year,
                totalJobs: 0,
                weeklyEarning: 0,
                jobs: [],
            };

            return responseModule.successResponse(res, {
                success: 1,
                message: 'Mover earning weeks fetched successfully.',
                data: { weeklyEarning: weekEmpty }
            });
        }
    }).catch(err => {
        console.log(err);
        logController.createLog('error', 'high', err, req.user.userType, req.user._id, {}, req.clientIp);
        return next({ msgCode: 2035 });
    });
};

let signUpStepThree = (req, res, next) => {
    let licenseFrontImage = req.body.licenseFrontImage,
        licenseBackImage = req.body.licenseBackImage,
        moverId = req.user._id;

    let filter = { _id: moverId },
        updatedObj = {};

    if ( licenseFrontImage && licenseFrontImage != '' ) {
        updatedObj.licenseFrontImage = licenseFrontImage;
    } else {
    	return next({ msgCode: 2038 });
    }

    if ( licenseBackImage && licenseBackImage != '' ) {
        updatedObj.licenseBackImage = licenseBackImage;
    }

    return contractorAccounts.findOneAndUpdate(filter, updatedObj, { new: true }).then(updatedMover => {
        if ( updatedMover ) {
            logController.createLog('info', 'normal', req.user.firstName + ' ' + req.user.lastName + ' updated his license info successfully', req.user.userType, req.user._id, {}, req.clientIp);

            let moverObj = userHelper.generateUserResponse(updatedMover);
            moverObj.dateOfBirth = commonLib.formatDate(moverObj.dateOfBirth);
             moverObj.licenseBackImage = updatedMover.licenseBackImage;
            moverObj.licenseFrontImage = updatedMover.licenseFrontImage;

            return responseModule.successResponse(res, {
                success: 1,
                message: 'Mover license info is updated successfully.',
                data: moverObj
            });
        } else {
            logController.createLog('error', 'high', req.user.firstName + ' ' + req.user.lastName + ' tried to update his license info but failed.', req.user.userType, req.user._id, {}, req.clientIp);
            return next({ msgCode: 5049 });
        }
    });
};

let getVerificationDocuments = (req, res, next) => {

    let $filter = { _id: _.trim(req.user._id) };
    let $fields = { licenseFrontImage: 1, licenseBackImage: 1 };
    return contractorAccounts.findOne($filter, $fields).then(licenceObj => {
        if ( licenceObj ) {
            logController.createLog('info', 'normal', req.user.firstName + ' ' + req.user.lastName + ' get his license info successfully', req.user.userType, req.user._id, {}, req.clientIp);
            return responseModule.successResponse(res, {
                success: 1,
                message: 'Mover license info fetched successfully.',
                data: licenceObj
            });
        } else {
            logController.createLog('error', 'high', req.user.firstName + ' ' + req.user.lastName + ' tried to get his license info but failed.', 'mover', null, {}, req.clientIp);
            return next({ msgCode: 2036 });
        }
    });
};

let getTermsAndConditions = (req, res, next) => {

    let filter = { contentType: 'termsAndCondition' };

    return contents.findOne(filter, { body: 1 }).then(termsConditions => {
        if ( termsConditions ) {
            return responseModule.successResponse(res, {
                success: 1,
                message: 'Terms and Conditions fetched successfully.',
                data: { termsAndCondition: termsConditions.body }
            });
        } else {
            return responseModule.successResponse(res, {
                success: 1,
                message: 'Terms and Conditions fetched successfully.',
                data: { termsAndCondition: '' }
            });
        }
    }).catch(err => {
        logController.createLog('error', 'high', 'Mover tried to get sign up Terms and Conditions failed.', 'mover', null, { err }, req.clientIp);
        return next({ msgCode: 2037 });
    });
};

let getRequirements = (req, res, next) => {
    let filter = { contentType: 'requirements' };

    return contents.find(filter, { body: 1 }).then(requirements => {
        if ( requirements.length > 0 ) {
            return responseModule.successResponse(res, {
                success: 1,
                message: 'Terms and Conditions fetched successfully.',
                data: { requirements: requirements }
            });
        } else {
            return responseModule.successResponse(res, {
                success: 1,
                message: 'Terms and Conditions fetched successfully.',
                data: { requirements: [] }
            });
        }
    }).catch(err => {
        logController.createLog('error', 'high', 'Mover tried to get sign up requirements but failed.', 'mover', req.user._id, { err }, req.clientIp);
        return next({ msgCode: 2037 });
    });
};

let getDisclosure = (req, res, next) => {
    let filter = { contentType: 'disclosure' };

    return contents.findOne(filter, { body: 1 }).then(disclosure => {
        if ( disclosure ) {
            return responseModule.successResponse(res, {
                success: 1,
                message: 'Terms and Conditions fetched successfully.',
                data: { disclosure: disclosure.body }
            });
        } else {
            return responseModule.successResponse(res, {
                success: 1,
                message: 'Terms and Conditions fetched successfully.',
                data: { disclosure: '' }
            });
        }
    }).catch(err => {
        logController.createLog('error', 'high', 'Mover tried to get sign up disclosure but failed.', req.user.userType, req.user._id, { err }, req.clientIp);
        return next({ msgCode: 2037 });
    });
};

module.exports = {
    isAccountExists,
    basicSignUp,
    signUpStepTwo,
    sendLoginCode,
    mediaUploaded,
    confirmAccount,
    logInMover,
    registerForPushNotification,
    moverLoginSuccess,
    getMoverStatus,
    logOutAccount,
    toggleMoverOnlineStatus,
    getCurrentMover,
    updateMoverProfile,
    getEarningWeeks,
    getDetailWeeklyEarningReport,
    signUpStepThree,
    getVerificationDocuments,
    getTermsAndConditions,
    getRequirements,
    getDisclosure
};