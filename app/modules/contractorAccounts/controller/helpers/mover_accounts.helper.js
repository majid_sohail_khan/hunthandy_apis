/**
 * Created by Waqar on 18/1/2018.
 */

const mongoose = require('mongoose'),
    contractorAccounts = mongoose.model('contractorAccounts');

let queryMover = (queryObject, projectObject) => {
    return contractorAccounts.findOne(queryObject, projectObject).then(moverFound => {
        return moverFound;
    }).catch(err => {
        throw { msgCode: 2025 };
    });
}

let updateMover = (queryObject, updatedObject) => {
    return contractorAccounts.findOneAndUpdate(queryObject, {$set: updatedObject}, {new: true}).then(updatedMover => {
        return updatedMover;
    }).catch(err => {
        throw { msgCode: 2032 };
    });
}

module.exports = {
    queryMover,
    updateMover
}