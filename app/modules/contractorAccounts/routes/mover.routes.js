/**
 * Created by Raza on 15/01/2018.
 */

const moverMiddleWare = require('../middlewares/mover.middleware'),
    moverController = require('../controller/mover.controller'),
    passport = require('../../../../config/passport'),
    multer = require('../../../../config/multer'),
    imageUpload = multer.upload(config.aws.s3.profileImageDirectory),
    commonLib = require('../../globals/global.library');


module.exports = (app, version) => {
    let moduleName = '/mover/';

    app.post(
        version + moduleName + 'signUp/basic',
        moverMiddleWare.validateBasicSignUp,
        moverController.isAccountExists,
        commonLib.fetchIPAdress,
        moverController.basicSignUp
    );

    app.post(
        version + moduleName + 'signUp/stepTwo',
        passport.isAuthenticated,
        passport.isAuthorized('mover'),
        moverMiddleWare.validateSignUpStepTwo,
        commonLib.fetchIPAdress,
        moverController.signUpStepTwo
    );

    app.post(
        version + moduleName + 'signUp/stepThree',
        passport.isAuthenticated,
        passport.isAuthorized('mover'),
        commonLib.fetchIPAdress,
        moverController.signUpStepThree
    );

    app.post(
        version + moduleName + 'verifyAccount',
        moverMiddleWare.confirmAccountValidate,
        commonLib.fetchIPAdress,
        moverController.confirmAccount
    );

    app.post(
        version + moduleName + 'uploadFile',
        imageUpload.array('image', 1),
        commonLib.fetchIPAdress,
        moverController.mediaUploaded
    );

    app.post(
        version + moduleName + 'sendLoginCode',
        moverMiddleWare.resendCodeValidate,
        commonLib.fetchIPAdress,
        moverController.sendLoginCode
    );

    app.post(
        version + moduleName + 'login',
        moverMiddleWare.logInValidate,
        moverController.logInMover,
        commonLib.enForceSingleSession,
        moverController.registerForPushNotification,
        commonLib.fetchIPAdress,
        moverController.moverLoginSuccess
    );

    app.get(
        version + moduleName + 'logout',
        passport.isAuthenticated,
        commonLib.fetchIPAdress,
        moverController.logOutAccount
    );

    app.get(
        version + moduleName + 'fetch/online/status',
        passport.isAuthenticated,
        passport.isAuthorized('mover'),
        moverController.getMoverStatus
    );

    app.post(
        version + moduleName + 'toggle/online/status',
        passport.isAuthenticated,
        passport.isAuthorized('mover'),
        moverMiddleWare.validateMoverOnline,
        moverController.toggleMoverOnlineStatus
    );

    app.get(
        version + moduleName + 'fetch/profile',
        passport.isAuthenticated,
        passport.isAuthorized('mover'),
        moverController.getCurrentMover
    );

    app.post(
        version + moduleName + 'update/profile',
        passport.isAuthenticated,
        passport.isAuthorized('mover'),
        moverMiddleWare.moverProfileValidation,
        commonLib.fetchIPAdress,
        moverController.updateMoverProfile
    );

    app.get(
        version + moduleName + 'weeks',
        passport.isAuthenticated,
        passport.isAuthorized('mover'),
        moverController.getEarningWeeks
    );

    app.get(
        version + moduleName + 'weekly/earning',
        passport.isAuthenticated,
        passport.isAuthorized('mover'),
        moverController.getDetailWeeklyEarningReport
    );

    app.get(
        version + moduleName + 'verification/documents',
        passport.isAuthenticated,
        passport.isAuthorized('mover'),
        moverController.getVerificationDocuments
    );

    // New Step Routes
    app.get(
        version + moduleName + 'signUp/termsAndConditions',
        commonLib.fetchIPAdress,
        moverController.getTermsAndConditions
    );

    app.get(
        version + moduleName + 'signUp/requirements',
        commonLib.fetchIPAdress,
        moverController.getRequirements
    );

    app.get(
        version + moduleName + 'signUp/disclosure',
        commonLib.fetchIPAdress,
        moverController.getDisclosure
    );
};