'use strict';

const mongoose = require('mongoose'),
    mongoose_timestamps = require('mongoose-timestamp'),
    schema = mongoose.Schema,
    winston = require('winston'),
    moment = require('moment');

let Address = {
    streetAddress: { type: String, default: '' },
    postalCode: { type: String, default: '' },
    city: { type: String, default: '' },
    state: { type: String, default: '' },
    aptNumber: { type: String, default: '' }
};

let jobSolt = {
    jobId: { type: schema.Types.ObjectId, required: true, ref: 'jobs' },
    startTime: { type: Number, default: 0 },
    endTime: { type: Number, default: 0 },
};

let contractorAccounts = new schema({
    name: { type: String, default: '' },
    firstName: { type: String, default: '' },
    lastName: { type: String, default: '' },
    socialSecurityNumber: { type: String, default: '' },
    gender: { type: String, default: '' },
    dateOfBirth: { type: Number, default: 0 },
    email: { type: String, required: true, index: { unique: true } },
    profileImage: {
        type: String,
        default: ''
    },
    phoneNumber: { type: String },
    bio: { type: String },
    deviceToken: { type: String, default: '' },
    deviceType: { type: String, default: '' },
    address: Address,
    currentLocation: {
        type: { type: String },
        coordinates: [ Number ]
    },
    isBusy: { type: Boolean, default: false },
    userType: { type: String, default: 'contractor' },
    servicesOffered: { type: [ String ], default: [] },
    isOnline: { type: Boolean, default: false },
    avgRating: { type: Number, default: 0 },
    hourlyRate: { type: Number, default: 0 },
    sessionId: { type: String, default: '' },
    stepCompleted: { type: Number, default: 0 },
    snsToken: { type: String, default: '' },
    isBlocked: { type: Boolean, default: false },
    vicTigOrderId: { type: String, default: null },
    vicTigReportUrl: { type: String, default: null },
    isReceiveBackGroundCheckReport: { type: Boolean, default: false }
});

contractorAccounts.plugin(mongoose_timestamps);
contractorAccounts.index({ name: 1 }, { background: true, name: 'IDX_MOVER_NAME' });
contractorAccounts.index({ currentLocation: '2dsphere' }, { background: true, name: 'IDX_MOVER_CURRENT_LOCATION' });
contractorAccounts.index({ email: 1 }, { background: true, name: 'IDX_MOVER_EMAIL' });

const userObject = mongoose.model('contractorAccounts', contractorAccounts);

module.exports = userObject;

