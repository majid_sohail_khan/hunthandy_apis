const winston = require('winston'),
    moment = require('moment'),
    logController = '',
    jsonKeys = require('../socketIo/socket_keys.json'),
    commonLib = require('../globals/global.library'),
    async = require('async'),
    config = require('../../../config/config.json'),
    request = require('request'),
    xml2js = require('xml2js'),
    parser = new xml2js.Parser(),

    mongoose = require('mongoose'),
    Weeks = '',
    jobs = '',
    settings = mongoose.model('settings'),
    jobNotifications = '',
    contractorAccounts = mongoose.model('contractorAccounts');

let checkWeeksIsExist = () => {
    winston.info('***************** Cron Job for WeekGenerations ***********************');
    let weekNumber = moment().isoWeek();
    let weekYear = moment().year();

    let filter = { weekNumber: weekNumber, weekYear: weekYear };
    Weeks.findOne(filter).then((week) => {
        if ( week ) {
            winston.info('***************** week already Exists: ' + week.title + ' ***********************');
            return;
        } else {
            winston.info('***************** Creating week ***********************');
            let weekStart = moment().startOf('isoweek').isoWeekday(1);
            let weekEnd = moment().startOf('isoweek').isoWeekday(7);
            let weekPeriod = '';

            if ( weekStart.month() === weekEnd.month() ) {
                weekPeriod = weekStart.date() + '-' + weekEnd.date() + ' ' + weekEnd.format('MMMM');
            } else {
                weekPeriod = weekStart.date() + ' ' + weekStart.format('MMMM') + '-' + weekEnd.date() + ' ' + weekEnd.format('MMMM');
            }

            let newWeek = {
                title: weekPeriod,
                weekNumber: weekNumber,
                weekYear: weekYear,
                weekStartDate: weekStart,
                weekEndDate: weekEnd,
            };
            const weekNew = new Weeks(newWeek);
            weekNew.save((err) => {
                if ( err ) {
                    winston.error(err);
                }
                winston.info('***************** Week Created : ' + weekNew.title + '  ***********************');
                return;
            });
        }
    }).catch((err) => {
        winston.error(err);
    });
};

let removeUnAssignedJobs = () => {
    let notificationsTime = moment().subtract(15, 'minutes');
    jobs.remove({ isOfferSent: false, createdAt: { $lte: notificationsTime }, }).exec();
};

let checkTimeOutJobOffer = () => {
    winston.info('***************** Cron Job for Job Offer TimeOut ***********************');
    return settings.findOne({}).then(settingsFound => {
        let jobOffersTimeInMinutes = settingsFound.jobOffersTimeInMinutes || 60;
        let notificationsTime = moment().subtract(jobOffersTimeInMinutes, 'minutes');
        let filter = {
            isAccepted: false,
            isRejected: false,
            createdAt: { $lte: notificationsTime },
            isCancelled: false,
            isTimeOut: false,
        };

        return jobNotifications.find(filter).populate('jobId userId', 'status orderDate address firstName lastName').lean().then(jobNotificationsFounds => {
            if ( jobNotificationsFounds ) {
                jobNotificationsFounds.forEach(jobNotification => {
                    if ( jobNotification.jobId !== null ) {
                        let notificationType = jsonKeys.jobTimeOut,
                            sockectData = {
                                jobId: jobNotification.jobId._id,
                                body: 'Oops! You’ve missed this job "' + jobNotification.jobId.address + '". Be quick next time.',
                            };
                        commonLib.sendSocketDataAndNotification(jsonKeys.jobTimeOut, null, jobNotification.moverId, 'mover', sockectData.body, notificationType, sockectData, sockectData);
                        jobNotifications.findOneAndUpdate({
                            jobId: jobNotification.jobId._id, moverId: jobNotification.moverId
                        }, { $set: { isTimeOut: true } }, { new: true }).exec();
                        commonLib.removeJobSlot(jobNotification.jobId._id, jobNotification.moverId, (err) => {
                            if ( err ) {
                                logController.createLog('error', 'high', err, 'system', 'cron', {}, null);
                            }
                        });
                    }
                });
            }
        }).catch(err => {
            logController.createLog('error', 'high', err, 'system', 'cron', {}, null);
        });


    });
};

let checkVicTigOrders = () => {
    winston.info('***************** Cron Job for VicTig Orders Check ***********************');
    return contractorAccounts.find({ vicTigOrderId: { $ne: null }, vicTigReportUrl: { $eq: null } }).then((movers) => {
        if ( movers ) {
            let xmlRequest = null;
            return async.forEachSeries(movers, (mover, cb) => {
                xmlRequest = '<?xml version=\'1.0\'?>' +
                    '<BackgroundCheck userId=\'' + config.vicTig.userId + '\' password=\'' + config.vicTig.password + '\'>' +
                    '<BackgroundSearchPackage action=\'searchstatus\'>' +
                    '<OrderId>' + mover.vicTigOrderId + '</OrderId>' +
                    '<Screenings>' +
                    '<Screening type=\'criminal\' qualifier=\'national\'>' +
                    '</Screening>' +
                    '</Screenings>' +
                    '</BackgroundSearchPackage>' +
                    '</BackgroundCheck>';

                request.post(
                    {
                        url: 'https://victig.instascreen.net/send/interchange',
                        body: xmlRequest,
                        headers: { 'Content-Type': 'text/xml' }
                    },
                    function (error, response, body) {
                        let url = null,
                            errorFound = null;
                        if ( !error && response.statusCode == 200 ) {
                            parser.parseString(body, function (err, result) {
                                url = result.BackgroundReports.BackgroundReportPackage[ 0 ].ReportURL[ 0 ];
                                contractorAccounts.findOneAndUpdate({ _id: mover._id }, { vicTigReportUrl: url }).exec();

                                if(url){
                                	let vars = {
                                		name: mover.name,
                                		vicTigReportUrl: url
                                	}
                                	commonLib.sendEmail(movers.email, 'MoveBig: VicTig Report', vars, 'victig_report', 'contractorAccountss')
                                }

                                winston.info('mover victig report saved successfully.');
                                cb();
                            });
                        } else {
                            parser.parseString(body, function (err, result) {
                                errorFound = result.BackgroundReports.BackgroundReportPackage[ 0 ].ErrorReport[ 0 ].ErrorDescription[ 0 ];
                                winston.error('error while fetching victig order.');
                                cb();
                            });
                        }

                    }
                );
            });
        }
    }).catch((err) => {
        console.log(err);
        winston.error('error while finding victig orders.');
    });

};

module.exports = {
    checkWeeksIsExist,
    removeUnAssignedJobs,
    checkTimeOutJobOffer,
    checkVicTigOrders
};