const _ = require('lodash'),
    async = require('async'),
    winston = require('winston'),
    moment = require('moment'),
    jsonKeys = require('../socketIo/socket_keys.json'),
    commonLib = require('../globals/global.library'),
    logController = '',
    stripeLib = '',

    mongoose = require('mongoose'),
    jobExtraHourRequestNotification = '',
    jobs = '',
    stripeConnectedAccounts = '';

let extraTimeRequestCron = () => {
    console.log('************************* Extra Time Request Cron ************************************');
    let filter = {
        expectedJobEndTime: { $lte: moment().add(15, 'minutes').utc().unix() },
        isNotificationSend: false,
        isJobPaused: false,
        isJobCompleted: false
    };

    jobExtraHourRequestNotification.find(filter).then(extraHourNotifications => {
        if ( extraHourNotifications.length > 0 ) {
            _.forEach(extraHourNotifications, notification => {
                let notificationType = jsonKeys.hurryUp,
                    socketData = {
                        jobId: notification.jobId,
                        body: 'Job time is running out (15 minutes) left.',
                    };

                commonLib.sendSocketDataAndNotification(notificationType, null, notification.moverId, 'mover', socketData.body, notificationType, socketData, socketData);

                jobExtraHourRequestNotification.findOneAndUpdate({ _id: notification._id }, { $set: { isNotificationSend: true } }, { new: true }).exec();
            });

        }

    }).catch(err => {
        winston.error((err.TypeError) ? err.TypeError : err);
    });
};

let sendFinishedJobEstimatedTimeCron = () => {
    console.log('************************* Estimated Time Completion Cron ************************************');
    let filter = {
        expectedJobEndTime: { $lte: moment().utc().unix() },
        isCompletedTimeNotificationSend: false,
        isJobPaused: false,
        isJobCompleted: false
    };

    jobExtraHourRequestNotification.find(filter)
        .populate('moverId', 'name firstName lastName')
        .populate('jobId', 'userId')
        .then(timeCompletionNotifications => {
            if ( timeCompletionNotifications.length > 0 ) {
                _.forEach(timeCompletionNotifications, notification => {
                	if(notification.jobId){
                    	jobs.findOne({ _id: notification.jobId._id }).then(job => {
                        	let index = job.movers.findIndex(mover => (mover.moverId.equals(notification.moverId._id) || mover.moverId.toString() === notification.moverId._id.toString() ));
                        	if ( index > -1 ) {
                        	    if ( job.movers[ index ].jobTimings.length > 0 ) {
                        	        let jobTimingSlots = job.movers[ index ].jobTimings;
                        	        let slotLength = jobTimingSlots.length - 1;
                        	        let lastSlot = jobTimingSlots[ slotLength ];
	
                        	        if ( lastSlot.endTime === 0 ) {
                        	            let jobStartTime = moment.unix(lastSlot.startTime);
                        	            let jobPauseTime = moment().utc();
	
                        	            let diff = moment.duration(jobPauseTime.diff(jobStartTime));
                        	            let minutes = Math.ceil(diff.asSeconds());
	
                        	            let jobId = _.trim(notification.jobId._id),
                        	                moverId = _.trim(notification.moverId._id),
                        	                filter = {
                        	                    _id: jobId,
                        	                    'movers.moverId': moverId,
                        	                    'movers.jobTimings._id': lastSlot._id
                        	                },
                        	                endTimeFieldName = 'movers.$.jobTimings.' + slotLength + '.endTime',
                        	                totalMinutesFieldName = 'movers.$.jobTimings.' + slotLength + '.totalMinutesSpend',
                        	                updatedObj = {
                        	                    $set: {
                        	                        'movers.$.isJobPaused': true,
                        	                        [endTimeFieldName]: jobPauseTime.unix(),
                        	                        [totalMinutesFieldName]: minutes
                        	                    }
                        	                };
	
                        	            return jobs.findOneAndUpdate(filter, updatedObj, { new: true }).then(updatedJob => {
                        	                if ( updatedJob ) {
                        	                    let notificationType = jsonKeys.jobTimeFinished,
                        	                        socketData = {
                        	                            jobId: notification.jobId._id,
                        	                            body: 'Your job time is finished. Request for extra hours if needed.',
                        	                        };
	
                        	                    commonLib.sendSocketDataAndNotification(notificationType, null, notification.moverId._id, 'mover', socketData.body, notificationType, socketData, socketData);
	
                        	                    let userSocketData = {
                        	                        jobId: notification.jobId._id, moverId: notification.moverId._id,
                        	                        body: notification.moverId.name + ' job time is over.',
                        	                    };
	
                        	                    commonLib.sendSocketDataAndNotification(notificationType, null, notification.jobId.userId, 'user', userSocketData.body, notificationType, userSocketData, userSocketData);
	
                        	                    jobExtraHourRequestNotification.findOneAndUpdate({ _id: notification._id }, {
                        	                        $set: {
                        	                            isCompletedTimeNotificationSend: true,
                        	                            isJobPaused: true
                        	                        }
                        	                    }, { new: true }).exec();
                        	                }
                        	            }).catch(err => {
                        	                logController.createLog('error', 'high', 'Error occurred while mover pause a job', 'system', null, { err: (err.TypeError) ? err.TypeError : err }, null);
                        	                winston.error((err.TypeError) ? err.TypeError : err);
                        	            });
                        	        } else {
                        	            jobExtraHourRequestNotification.findOneAndUpdate({ _id: notification._id }, {
                        	                $set: {
                        	                    isCompletedTimeNotificationSend: true,
                        	                    isJobPaused: true
                        	                }
                        	            }, { new: true }).exec();
                        	        }
                        	    }
                        	}
	                    });
					}
                });
            }
        })
        .catch(err => {
            winston.error((err.TypeError) ? err.TypeError : err);
        });
};

let removedUnAcceptedJobs = () => {
    console.log('************************* UnAccepted Jobs Removal Cron ************************************');
    let filter = {
        orderDate: { $lte: moment().subtract(180, 'minutes').utc().unix() },
        status: 'Open', movers: { $exists: true, $eq: [] }
    };

    jobs.find(filter).then(openedJobs => {
        if ( openedJobs.length >= 1 ) {
            _.forEach(openedJobs, job => {
                jobs.findOneAndUpdate({ _id: job._id }, { isArchived: true, status: 'Expired' }, { new: true }).exec();
            });
        }
    });
};

let updateMoverStripePayout = (req, res, next) => {
    winston.log('info', '*************************** Update Mover Stripe Account ********************************');
    stripeConnectedAccounts.find({ 'payoutSchedule.weekly_anchor': 'monday' }).then(oldAccounts => {
        if ( oldAccounts ) {
            let count = 0;
            async.whilst(
                () => {
                    return count < oldAccounts.length;
                },
                (connectedAccountCb) => {
                    console.log(oldAccounts[ count ]);
                    let stripeConnectedAccountId = oldAccounts[ count ].connectedAccountId;
                    stripeLib.updateConnectedAccount(stripeConnectedAccountId, {
                        payout_schedule: { interval: 'weekly', weekly_anchor: 'friday' }
                    }, (err, updatedAccount) => {
                        if ( err ) {
                            connectedAccountCb(err);
                        }
                        if ( updatedAccount ) {
                            let filter = { connectedAccountId: stripeConnectedAccountId };
                            stripeConnectedAccounts.findOneAndUpdate(filter, { $set: { 'payoutSchedule.weekly_anchor': 'friday' } }, { new: true }).exec();
                        }
                        count++;
                        connectedAccountCb();
                    });
                },
                (err) => {
                    if ( err ) {
                        winston.error((err.TypeError) ? err.TypeError : err);
                    }
                }
            );
        }
    }).catch(err => {
        console.log(err);
        winston.error((err.TypeError) ? err.TypeError : err);
    });
};

module.exports = {
    extraTimeRequestCron,
    sendFinishedJobEstimatedTimeCron,
    removedUnAcceptedJobs,
    updateMoverStripePayout
};