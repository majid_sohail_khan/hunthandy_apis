/**
 * Created by Asif on 8/12/2017.
 */


/**
 * Created by Asif on 6/21/2017.
 */
'use strict';

const mongoose = require('mongoose'),
    mongoose_timestamps = require('mongoose-timestamp'),
    schema = mongoose.Schema;


let userAccount = new schema({
    name: { type: String, required: true },
    firstName: { type: String, required: true },
    lastName: { type: String, required: true },
    email: { type: String, required: true, index: { unique: true } },
    profileImage: { type: String },
    phoneNumber: { type: String },
    gender: {type: String, enum: config.genderTypes},
    dateOfBirth: {type: Date},
    confirmationCode: { type: String },
    confirmationCodeExpirationTime: { type: Date },
    deviceToken: { type: String },
    deviceType: { type: String, enum: config.deviceTypes },
    address: {type: String},
    city: { type: String },
    state: { type: String },
    postalCode: { type: String },
    userType: { type: String, required: true, enum: config.userTypes },
    socialSecurityNumber: {type: String, default: ''},
    isBlocked: { type: Boolean, default: false }
});

userAccount.plugin(mongoose_timestamps);

mongoose.model('tempUserAccount', userAccount);


