/**
 * Created by Asif on 7/11/2017.
 */

'use strict'; // NOSONAR

let mongoose = require('mongoose');
let Schema = mongoose.Schema;
let pnd = new Schema({
    user: {
        type: Schema.ObjectId,
        ref: 'User',
        required: 'User is required'
    },
    sid: {
        type: String,
        required: 'sid is required'
    },
    arn: {
        type: String,
        required: 'arn is required'
    },
    type: { type: Number, enum: [1, 2] }, // 1 for IOS 2 for Android
    token: {
        type: String,
        required: 'deviceToken is missing'
    }

});

mongoose.model('pushNotifications', pnd);