/**
 * Created by Asif on 6/21/2017.
 */
'use strict';

const mongoose = require('mongoose'),
    mongoose_timestamps = require('mongoose-timestamp'),
    schema = mongoose.Schema,
    winston = require('winston');

let Address = {
    streetAddress: { type: String, default: '' },
    postalCode: { type: String, default: '' },
    city: { type: String, default: '' },
    state: { type: String, default: '' },
    aptNumber: { type: String, default: '' }
};

let userAccounts = new schema({
    name: { type: String, default: '' },
    firstName: { type: String, default: '' },
    lastName: { type: String, default: '' },
    socialSecurityNumber: { type: String, default: '' },
    gender: { type: String, default: '' },
    dateOfBirth: { type: Number, default: 0 },
    email: { type: String, required: true, index: { unique: true } },
    profileImage: {
        type: String,
        default: ''
    },
    phoneNumber: { type: String },
    bio: { type: String },
    deviceToken: { type: String, default: '' },
    deviceType: { type: String, default: '' },
    address: Address,
    currentLocation: {
        type: { type: String },
        coordinates: [ Number ]
    },
    userType: { type: String, default: 'contractor' },
    isOnline: { type: Boolean, default: false },
    avgRating: { type: Number, default: 0 },
    sessionId: { type: String, default: '' },
    stepCompleted: { type: Number, default: 0 },
    snsToken: { type: String, default: '' },
    isBlocked: { type: Boolean, default: false },
    isFacebookAccount: { type: Boolean, default: false },
    facebookId: { type: String, default: '' },
    facebookToken: { type: String, default: '' },
    facebookEmailUpdate: { type: Boolean, default: false },
    facebookPhoneNumberUpdate: { type: Boolean, default: false },
});

userAccounts.plugin(mongoose_timestamps);
userAccounts.index({ name: 1 }, { background: true, name: 'IDX_USERNAME' });
userAccounts.index({ currentLocation: '2dsphere' }, { name: 'IDX_USER_CURRENT_LOCATION' });
userAccounts.index({ email: 1 }, { background: true, name: 'IDX_USER_NAME' });

const userObject = mongoose.model('userAccounts', userAccounts);

module.exports = userObject;

