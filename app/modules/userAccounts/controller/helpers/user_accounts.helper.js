/**
 * Created by Waqar on 1/8/2018.
 */

const randomize = require('randomatic'),
    mongoose = require('mongoose'),
    userAccounts = mongoose.model('userAccounts'),
    tempuserAccounts = mongoose.model('tempUserAccount');

let generateCustomCode = (codeLength, expiryMinutes) => {
    const expTime = new Date();

    return { code: randomize('0', codeLength), expiryTime: addDays(expTime, expiryMinutes) };
};

let addDays = (date, days) => {
    var result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
};

let generateVerificationCode = (codeLength, expiryMinutes) => {
    let verificationCode = randomize('0', config.verificationCodeLength);
    let expTime = new Date();
    let verificationCodeExpiryTime = expTime.setMinutes(expTime.getMinutes() + config.verifyAcctCodeExpiryMinutes);
    return { code: verificationCode, expiryTime: verificationCodeExpiryTime };
};

let generateUserResponse = user => {
    return {
        userId: user._id,
        userName: user.name,
        firstName: user.firstName,
        lastName: user.lastName,
        email: user.email,
        phoneNumber: user.phoneNumber || '',
        userType: user.userType,
        gender: user.gender,
        dateOfBirth: user.dateOfBirth,
        address: (user.address) ? user.address : '',
        city: (user.city) ? user.city : '',
        state: (user.state) ? user.state : '',
        postalCode: (user.postalCode) ? user.postalCode : '',
        profileImage: user.profileImage,
        servicesOffered: user.servicesOffered || '',
        bio: user.bio || '',
        hourlyRate: user.hourlyRate || '',
        socialSecurityNumber: user.socialSecurityNumber,
        isFacebookAccount: user.isFacebookAccount || false,
        facebookId: user.facebookId || '',
        facebookEmailUpdate: user.facebookEmailUpdate || false,
        facebookPhoneNumberUpdate: user.facebookPhoneNumberUpdate || false,
    };
};

let queryUser = (queryObject, projectObject) => {
    return userAccounts.findOne(queryObject, projectObject).then(userFound => {
        return userFound;
    }).catch(err => {
        throw { msgCode: 5043 };

    });
};

let queryTempUser = (queryObject, projectObject) => {
    return tempuserAccounts.findOne(queryObject, projectObject).then(userFound => {
        return userFound;
    }).catch(err => {
        throw { msgCode: 5044 };

    });
};

module.exports = {
    generateCustomCode,
    generateVerificationCode,
    addDays,
    generateUserResponse,
    queryUser,
    queryTempUser
};