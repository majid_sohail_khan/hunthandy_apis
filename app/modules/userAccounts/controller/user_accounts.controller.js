const _ = require('lodash'),
    async = require('async'),
    passport = require('passport'),
    mongoose = require('mongoose'),
    moment = require('moment'),
    NodeGeocoder = require('node-geocoder'),
    winston = require('winston'),

    responseModule = require('../../../../config/response'),
    sms = require('../../../../config/sms'),
    multer = require('../../../../config/multer'),
    pushNotification = require('../../../../config/notifications'),

    commonLib = require('../../globals/global.library'),
    logController = '',
    jsonKeys = require('../../socketIo/socket_keys.json'),

    userHelper = require('./helpers/user_accounts.helper'),

    jobs = '',
    jobNotifications = '',
    moverAccount = '',
    ratings = '',
    tempuserAccounts = mongoose.model('tempUserAccount'),
    userAccounts = mongoose.model('userAccounts'),
    settings = mongoose.model('settings'),

    options = {
        provider: 'google',
        // Optional depending on the providers
        httpAdapter: 'https', // Default
        apiKey: config.googleMapsAPI, // for Mapquest, OpenCage, Google Premier
        formatter: null         // 'gpx', 'string', ...
    },
    geocoder = NodeGeocoder(options);

let mediaUploaded = (req, res, next) => {
    try {
        multer.resizeAndUpload(req.files[ 0 ].location, req.files[ 0 ].key.split('1000X1000/')[ 1 ]);
        return responseModule.successResponse(res, {
            success: 1,
            message: '',
            data: { url: req.files[ 0 ].location }
        });
    }
    catch ( err ) {
        winston.error(err);
        return next({ msgCode: 5034 });
    }
};

let logInUser = (req, res, next) => {
    passport.authenticate('user', (err, user, info) => {
        if ( err ) {
            return next({ msgCode: 5036 });
        }
        if ( !user ) {
            return next({ msgCode: 5045 });
        }
        req.logIn(user, (err) => {
            if ( err ) {
                return next({ msgCode: 5036 });
            }
            req.acct = user;
            next();
        });
    })(req, res, next);
};

let registerForPushNotification = (req, res, next) => {
    if ( config.deviceTypes.indexOf(req.body.deviceType) > -1 ) {
        if ( req.body.deviceType && req.body.deviceToken ) {
            pushNotification.generateEndPoint(req).then(endPoint => {
                if ( endPoint ) {
                    let filter = { _id: req.user._id },
                        update = {
                            deviceType: req.body.deviceType,
                            deviceToken: req.body.deviceToken,
                            snsToken: endPoint
                        };

                    return userAccounts.findOneAndUpdate(filter, { $set: update }, { new: true }).then(userFound => {
                        if ( userFound ) {
                            return next();
                        } else {
                            return next({ msgCode: 5055 });
                        }
                    });
                } else {
                    return next({ msgCode: 5056 });
                }
            });
        } else {
            return next({ msgCode: 5057 });
        }
    } else {
        return next({ msgCode: 5046 });
    }
};

let userLoginSuccess = (req, res, next) => {
    let userObj = userHelper.generateUserResponse(req.acct);
    userObj.dateOfBirth = commonLib.formatDate(userObj.dateOfBirth);
    responseModule.successResponse(res, {
        success: 1,
        message: 'User logged in successfully.',
        data: userObj ? userObj : {}
    });
};

let sendSignUpVerification = (req, res, next) => {
    const email = _.trim(req.body.email).toLowerCase(),
        firstName = _.trim(req.body.firstName),
        lastName = _.trim(req.body.lastName),
        name = firstName + ' ' + lastName,
        profileImage = _.trim(req.body.profileImage) || '',
        deviceType = _.trim(req.body.deviceType),
        deviceToken = _.trim(req.body.deviceToken),
        phoneNumber = req.body.phoneNumber,
        address = req.body.address || '',
        city = req.body.city || '',
        state = req.body.state || '',
        postalCode = req.body.postalCode || '';

    return userHelper.queryUser({ $or: [ { phoneNumber: phoneNumber }, { email: email } ] }, {
        phoneNumber: 1,
        email: 1
    }).then(userFound => {
        if ( userFound ) {
            if ( email === userFound.email ) {
                throw { msgCode: 5009 };
            }
            else if ( phoneNumber === userFound.phoneNumber ) {
                throw { msgCode: 5010 };
            }
            else {
                throw 'There was error creating user.';
            }
        }
        else {
            // if ( address || city || state ) {
            //     return geocoder.geocode(address + ' ' + city + ' ' + state + ' US').then(geoResponse => {
            //         if ( !geoResponse.length ) {
            //             throw { msgCode: 5031 };
            //         }
            //         if ( geoResponse.length > 0 ) {
            //             if ( geoResponse[ 0 ].zipcode ) {
            //                 if ( geoResponse[ 0 ].zipcode != req.body.postalCode ) {
            //                     throw { msgCode: 5031 };
            //                 }
            //             }
            //         }
            let codeObject = userHelper.generateVerificationCode(config.confirmationCodeLength, config.confirmAcctCodeExpiryMinutes);
            let tempUserCreateObject = {
                confirmationCode: codeObject.code,
                confirmationCodeExpirationTime: codeObject.expiryTime,
                firstName: firstName,
                lastName: lastName,
                name: name,
                profileImage: profileImage,
                deviceType: deviceType,
                deviceToken: deviceToken,
                phoneNumber: phoneNumber,
                email: email,
                address: address,
                city: city,
                userType: 'user',
                state: state,
                postalCode: postalCode
            };

            if ( req.body.gender ) {
                tempUserCreateObject.gender = req.body.gender;
            }

            if ( req.body.dateOfBirth ) {
                tempUserCreateObject.dateOfBirth = commonLib.getUTCDateTime(req.body.dateOfBirth);
            }


            if ( !profileImage ) {
                delete tempUserCreateObject.profileImage;
            }

            return tempuserAccounts.findOneAndUpdate({ email: tempUserCreateObject.email }, { $set: tempUserCreateObject }, {
                upsert: true,
                new: true
            });
            //     });
            // }
        }
    }).then(tempUserCreated => {
        async.series([
            (SMSCb) => {
                let smsObject = {
                    to: phoneNumber,
                    text: 'MoveBig: ' + 'Verification code' + ' is ' + tempUserCreated.confirmationCode,
                };
                sms.sendMessage(smsObject, (err) => {
                    if ( err ) {
                        return SMSCb('Error: Verification Code SMS not sent due to technical reasons. Please try later.');
                    }
                    else {
                        return SMSCb();
                    }
                });
            },
            (mailCb) => {
                let vars = {
                        verificationCode: tempUserCreated.confirmationCode,
                        name: tempUserCreated.name
                    },
                    subject = 'MoveBig: Account verification';
                commonLib.sendEmail(tempUserCreated.email, subject, vars, 'account_verification', 'userAccountss').then(() => {
                    mailCb();
                }).catch(err => {
                    mailCb(err);
                });
            }
        ], (err) => {
            if ( err ) {
                return next({ msgCode: 5037 });
            }
            else {
                return responseModule.successResponse(res, {
                    success: 1,
                    message: 'A message with a verification code has been sent to your email and phone.',
                    data: { code: tempUserCreated.confirmationCode }
                });
            }
        });
    }).catch(err => {
        if ( err.msgCode ) {
            return next(err);
        } else {
            return next({ msgCode: 5038 });
        }
    });
};

let confirmAccount = (req, res, next) => {
    let phoneNumber = _.trim(req.body.phoneNumber);
    let filter = { phoneNumber: _.trim(phoneNumber) };
    return userHelper.queryUser(filter).then(userFound => {
        if ( userFound ) {
            return next({ msgCode: 5050 });
        }
        return userHelper.queryTempUser(filter).then(tempUserFound => {
            if ( !tempUserFound ) {
                return next({ msgCode: 5012 });
            }

            if ( parseInt(tempUserFound.confirmationCode) !== parseInt(req.body.confirmationCode) ) {
                return next({ msgCode: 5013 });
            }

            if ( tempUserFound.confirmationCodeExpirationTime <= new Date() ) {
                return next({ msgCode: 5014 });
            }
            let createUserObject = {
                firstName: tempUserFound.firstName,
                lastName: tempUserFound.lastName,
                name: tempUserFound.name,
                gender: tempUserFound.gender,
                dateOfBirth: tempUserFound.dateOfBirth,
                profileImage: tempUserFound.profileImage,
                deviceType: tempUserFound.deviceType,
                deviceToken: tempUserFound.deviceToken,
                phoneNumber: tempUserFound.phoneNumber,
                email: tempUserFound.email,
                isPhoneVerified: true,
                userType: 'user',
                address: tempUserFound.address,
                city: tempUserFound.city,
                state: tempUserFound.state,
                postalCode: tempUserFound.postalCode
            };

            new userAccounts(createUserObject).save().then(userCreated => {
                tempuserAccounts.remove({ phoneNumber: phoneNumber }).exec();

                req.login(userCreated, err => {
                    if ( err ) {
                        return next({ msgCode: 5036 });
                    }

                    if ( req.body.deviceType && req.body.deviceToken ) {
                        pushNotification.generateEndPoint(req).then(endPoint => {
                            if ( endPoint ) {
                                let filter = { _id: req.user._id },
                                    update = {
                                        deviceType: req.body.deviceType,
                                        deviceToken: req.body.deviceToken,
                                        snsToken: endPoint
                                    };

                                return userAccounts.findOneAndUpdate(filter, { $set: update }, { new: true }).then(userFound => {
                                    if ( !userFound ) {
                                        return next({ msgCode: 5055 });
                                    }
                                });
                            } else {
                                return next({ msgCode: 5056 });
                            }
                        });
                    } else {
                        return next({ msgCode: 5057 });
                    }

                    let userObj = userHelper.generateUserResponse(userCreated);
                    userObj.dateOfBirth = commonLib.formatDate(userCreated.dateOfBirth);

                    commonLib.sendEmail(userCreated.email, 'Welcome to MoveBig!', {}, 'welcome', 'userAccountss')
                    .catch(err => {
                        winston.error(err);
                    });  

                    return responseModule.successResponse(res, {
                        success: 1,
                        message: 'User verified successfully.',
                        data: userObj
                    });
                });
            });
        });
    }).catch(err => {
        return next({ msgCode: 5039 });
    });
};

let resendVerificationCode = (req, res, next) => {
    let codeGeneration = userHelper.generateVerificationCode(config.confirmationCodeLength, config.confirmAcctCodeExpiryMinutes);

    let userPhoneNumber = _.trim(req.body.phoneNumber),
        confirmationCode = codeGeneration.code,
        confirmationCodeExpirationTime = codeGeneration.expiryTime;

    let updateObject = {
        loginCode: confirmationCode,
        loginCodeExpirationTime: confirmationCodeExpirationTime
    };

    return userAccounts.findOneAndUpdate({ phoneNumber: userPhoneNumber }, { $set: updateObject }, { new: true }).then(userUpdated => {
        if ( userUpdated ) {
            async.series([
                (SMSCb) => {

                    let smsObject = {
                        to: userUpdated.phoneNumber,
                        text: 'MoveBig: ' + 'Verification code' + ' is ' + confirmationCode,
                    };

                    sms.sendMessage(smsObject, (err) => {
                        if ( err ) {
                            return SMSCb('SMS not sent due to technical reasons. Please try later. ');
                        }
                        else {
                            return SMSCb();
                        }
                    });
                },
                (mailCb) => {
                    let vars = {
                            confirmationCode: confirmationCode,
                            name: userUpdated.name
                        },
                        subject = 'MoveBig: Account verification';
                    commonLib.sendEmail(userUpdated.email, subject, vars, 'login', 'userAccountss').then(() => {
                        mailCb();
                    }).catch(err => {
                        mailCb(err);
                    });
                }
            ], (err) => {
                if ( err ) {
                    return next({ msgCode: 5037 });
                }
                else {
                    return responseModule.successResponse(res, {
                        success: 1,
                        message: 'Verification code resent successfully.',
                        data: {}
                    });
                }
            });
        } else {
            let updateObject = {
                confirmationCode: confirmationCode,
                confirmationCodeExpirationTime: confirmationCodeExpirationTime
            };

            return tempuserAccounts.findOneAndUpdate({ phoneNumber: userPhoneNumber }, { $set: updateObject }, { new: true }).then(tempUserUpdated => {
                if ( tempUserUpdated ) {
                    async.series([
                        (SMSCb) => {
                            let smsObject = {
                                to: tempUserUpdated.phoneNumber,
                                text: 'MoveBig: ' + 'Verification code' + ' is ' + confirmationCode,
                            };

                            sms.sendMessage(smsObject, (err) => {
                                if ( err ) {
                                    return SMSCb('SMS not sent due to technical reasons. Please try later. ');
                                } else {
                                    return SMSCb();
                                }
                            });
                        },
                        (mailCb) => {
                            let vars = {
                                    verificationCode: confirmationCode,
                                    name: tempUserUpdated.name
                                },
                                subject = 'MoveBig: Account verification';
                            commonLib.sendEmail(tempUserUpdated.email, subject, vars, 'account_verification', 'userAccountss').then(() => {
                                mailCb();
                            }).catch(err => {
                                mailCb(err);
                            });
                        }
                    ], (err) => {
                        if ( err ) {
                            return next({ msgCode: 5037 });
                        }
                        else {
                            return responseModule.successResponse(res, {
                                success: 1,
                                message: 'Verification code resent successfully.',
                                data: {}
                            });
                        }
                    });
                } else {
                    throw { msgCode: 5032 };
                }
            })
        }
    }).catch(err => {
        return next({ msgCode: 5040 });
    });
};

let sendLoginCode = (req, res, next) => {
    let codeGeneration = userHelper.generateVerificationCode(config.confirmationCodeLength, config.confirmAcctCodeExpiryMinutes);

    let userPhoneNumber = _.trim(req.body.phoneNumber),
        confirmationCode = codeGeneration.code,
        confirmationCodeExpirationTime = codeGeneration.expiryTime;

    let updateObject = {
        loginCode: confirmationCode,
        loginCodeExpirationTime: confirmationCodeExpirationTime
    };

    return userAccounts.findOneAndUpdate({ phoneNumber: userPhoneNumber }, { $set: updateObject }, { new: true }).then(userUpdated => {
        if ( userUpdated ) {
            if ( userUpdated.isBlocked ) {
                return next({ msgCode: '0007' });
            } else {
                async.series([
                    (SMSCb) => {

                        let smsObject = {
                            to: userUpdated.phoneNumber,
                            text: 'MoveBig: ' + 'Login code' + ' is ' + confirmationCode,
                        };

                        sms.sendMessage(smsObject, (err) => {
                            if ( err ) {
                                return SMSCb('Verification Code SMS not sent due to technical reasons. Please try later. ');
                            }
                            else {
                                return SMSCb();
                            }
                        });
                    },
                    (mailCb) => {
                        let vars = {
                                confirmationCode: confirmationCode,
                                name: userUpdated.name
                            },
                            subject = 'MoveBig: Login Code';
                        commonLib.sendEmail(userUpdated.email, subject, vars, 'login', 'userAccountss').then(() => {
                            mailCb();
                        }).catch(err => {
                            mailCb('There was some error in sending email.');
                        });
                    }
                ], (err) => {
                    if ( err ) {
                        return next({ msgCode: 5037 });
                    }
                    else {
                        return responseModule.successResponse(res, {
                            success: 1,
                            message: 'Login code sent successfully.',
                            data: { code: confirmationCode, phoneNumber: userPhoneNumber }
                        });
                    }
                });
            }

        } else {
            throw { msgCode: 5032 };
        }
    }).catch(err => {
        return next({ msgCode: 5032 });
    });
};

let logOutAccount = (req, res, next) => {
    let sid = req.sessionID;
    let user = req.user;
    pushNotification.removeEndPointUsingSessionId(sid);

    req.session.destroy(function (err) {
        req.logout();
    });

    moverAccount.findOneAndUpdate({ _id: _.trim(user._id) }, {
        $set: {
            deviceType: '',
            deviceToken: '',
            snsToken: ''
        }
    }, { new: true }).exec();

    responseModule.successResponse(res, {
        success: 1,
        message: 'User logged out successfully.',
        data: {}
    });
};

let fetchMovers = (req, res, next) => {
    let lat = parseFloat(req.body.lat),
        long = parseFloat(req.body.long);

    const maxDistance = (req.body.hasOwnProperty('moverRadius')) ? parseInt(req.body.moverRadius) : config.jobRadius * 1000;

    let filters = {
        currentLocation: {
            $near: {
                $geometry: {
                    type: 'Point',
                    coordinates: [ long, lat ]
                },
                $minDistance: 0,
                $maxDistance: maxDistance
            }
        },
        isOnline: true,
        isBlocked: false,
        adminVerified: true,
        isConnectedAccountExists: true
    };

    let fields = {
        profileImage: 1,
        currentLocation: 1,
        bio: 1,
        avgRating: 1,
        hourlyRate: 1,
        firstName: 1,
        lastName: 1
    };

    return moverAccount.find(filters, fields).limit(20).lean().then(moversFound => {
        if ( moversFound.length ) {
            moversFound.forEach(mover => {
                mover.firstName = commonLib.capitalizeFirstLetter(mover.firstName);
                mover.lastName = commonLib.capitalizeFirstLetter(mover.lastName);
                mover.currentLocation.coordinates = commonLib.invertArray(mover.currentLocation.coordinates);
            });
        }

        return responseModule.successResponse(res, {
            success: 1,
            message: 'Movers fetched successfully.',
            data: {
                moversFound
            }
        });
    });
};

let fetchSuggestedMovers = (req, res, next) => {
    let lat = parseFloat(req.body.lat),
        long = parseFloat(req.body.long),
        jobStatus = req.body.jobStatus,
        jobId = req.body.jobId;

    const maxDistance = (req.body.hasOwnProperty('moverRadius')) ? parseInt(req.body.moverRadius) : config.jobRadius * 1000;

    let jobFilter = { _id: jobId };
    return jobs.findOne(jobFilter).then(jobFound => {
        if ( jobFound ) {
            let jobNotificationFilter = { jobId: jobId };
            const startTime = jobFound.startTime;
            const endTime = moment.unix(jobFound.startTime).add(jobFound.estimatedHours, 'hours').unix();

            return jobNotifications.find(jobNotificationFilter).then(jobNotificationsFound => {

                let moverIds = jobNotificationsFound.map(({ moverId }) => moverId);
                let filters = {
                    currentLocation: {
                        $near: {
                            $geometry: {
                                type: 'Point',
                                coordinates: [ long, lat ]
                            },
                            $minDistance: 0,
                            $maxDistance: maxDistance
                        }
                    },
                    isOnline: true,
                    isBlocked: false,
                    adminVerified: true,
                    isConnectedAccountExists: true,
                    hourlyRate: { $gt: 0 },
                    servicesOffered: {
                        $in: jobFound.jobType
                    },
                    _id: {
                        $nin: moverIds
                    }
                };

                let fields = {
                    profileImage: 1,
                    currentLocation: 1,
                    bio: 1,
                    avgRating: 1,
                    hourlyRate: 1,
                    firstName: 1,
                    lastName: 1,
                    servicesOffered: 1,
                    jobSlots: 1
                };

                return moverAccount.find(filters, fields).limit(50).lean().then(moversFound => {
                    if ( moversFound.length ) {
                        moversFound.forEach(mover => {

                        });

                        let i = 0;
                        async.whilst(() => {
                                return i < moversFound.length;
                            },
                            (loopCb) => {
                                try {
                                    let slotIndex = _.findIndex(moversFound[ i ].jobSlots, (jobSlot) => {
                                        return (jobSlot.startTime <= startTime && jobSlot.endTime >= startTime) || (startTime <= jobSlot.startTime && endTime >= jobSlot.startTime ) || (jobSlot.startTime >= startTime && jobSlot.startTime <= endTime && jobSlot.endTime <= endTime);
                                    });

                                    if ( slotIndex >= 0 ) {
                                        moversFound.splice(i, 1);
                                        loopCb();
                                    } else {
                                        moversFound[ i ].firstName = commonLib.capitalizeFirstLetter(moversFound[ i ].firstName);
                                        moversFound[ i ].lastName = commonLib.capitalizeFirstLetter(moversFound[ i ].lastName);
                                        moversFound[ i ].currentLocation.coordinates = commonLib.invertArray(moversFound[ i ].currentLocation.coordinates);
                                        moversFound[ i ].hourlyRate = commonLib.addCompanyPercentage(moversFound[ i ].hourlyRate, req.settings);

                                        i++;
                                        loopCb();
                                    }
                                } catch ( err ) {
                                    loopCb(err);
                                }
                            },
                            (err) => {
                                if ( err ) {
                                    logController.createLog('error', 'high', err, req.user.userType, req.user._id, {}, req.clientIp);
                                    return next({ msgCode: 5054 });
                                } else {

                                    return responseModule.successResponse(res, {
                                        success: 1,
                                        message: 'Movers fetched successfully.',
                                        data: {
                                            moversFound
                                        }
                                    });
                                }
                            }
                        );

                    } else {
                        if ( jobStatus == 'new' ) {
                            userAccounts.findOne({ userType: { $eq: 'admin' } }, {
                                email: 1,
                            }).then(adminFound => {
                                let notificationMessage = 'We couldn\'t find any mover. Please review the job.',
                                    notificationType = jsonKeys.noMoverFound,
                                    adminData = {
                                        jobId: jobId
                                    };
                                commonLib.sendSocketDataAndNotification(jsonKeys.noMoverFound, null, adminFound._id, 'admin', notificationMessage, notificationType, adminData, adminData);
                            });
                        }

                        return responseModule.successResponse(res, {
                            success: 1,
                            message: 'Movers fetched successfully.',
                            data: {
                                moversFound
                            }
                        });
                    }


                }).catch((err) => {
                    logController.createLog('error', 'high', err, req.user.userType, req.user._id, { err: (err.TypeError) ? err.TypeError : err }, req.clientIp);
                    return next({ msgCode: 5054 });
                });
            });
        } else {
            return next({ msgCode: 7009 });
        }
    });
};

let fetchMoverProfile = (req, res, next) => {
    let moverId = req.params.id;

    let filters = {
        _id: moverId
    };
    let fields = {
        profileImage: 1,
        currentLocation: 1,
        bio: 1,
        avgRating: 1,
        hourlyRate: 1,
        firstName: 1,
        lastName: 1,
        email: 1,
        gender: 1,
        dateOfBirth: 1,
        servicesOffered: 1,
        address: 1
    };

    return moverAccount.findOne(filters, fields).lean().then(moverFound => {
        if ( moverFound.dateOfBirth ) {
            moverFound.dateOfBirth = commonLib.formatDate(moverFound.dateOfBirth);
        }

        if ( moverFound.gender ) {
            moverFound.gender = commonLib.capitalizeFirstLetter(moverFound.gender);
        }

        moverFound.firstName = commonLib.capitalizeFirstLetter(moverFound.firstName);
        moverFound.lastName = commonLib.capitalizeFirstLetter(moverFound.lastName);

        moverFound.currentLocation.coordinates = commonLib.invertArray(moverFound.currentLocation.coordinates);
        moverFound.servicesOffered = commonLib.sortArray(moverFound.servicesOffered);
        moverFound.servicesOffered = commonLib.convertArrayToString(moverFound.servicesOffered);
        moverFound.hourlyRate = commonLib.addCompanyPercentage(moverFound.hourlyRate);

        let ratingFilters = {
            moverId: moverId
        };
        let ratingsFields = {
            rating: 1,
            description: 1
        };
        return ratings.find(ratingFilters, ratingsFields).sort({ createdAt: -1 }).then(ratingsFound => {
            moverFound.ratings = ratingsFound.slice(0, 2);
            return responseModule.successResponse(res, {
                success: 1,
                message: 'Movers fetched successfully.',
                data: moverFound
            });
        });
    }).catch(err => {
        logController.createLog('error', 'high', req.user.firstName + ' ' + req.user.lastName + ' tried to update his profile but failed.', req.user.userType, req.user._id, { err: (err.TypeError) ? err.TypeError : err }, req.clientIp);
        return next({ msgCode: 5049 });
    });
};

let fetchMoverRatings = (req, res, next) => {
    let moverId = req.params.id;
    let offset = (req.query.offset) ? parseInt(req.query.offset) : 0;
    let limit = (req.query.limit) ? parseInt(req.query.limit) : 20;
    let filters = {
        moverId: moverId
    };
    let fields = {
        rating: 1,
        description: 1
    };

    return ratings.find(filters, fields).sort({ createdAt: -1 }).skip(offset).limit(limit).then(ratings => {
        return responseModule.successResponse(res, {
            success: 1,
            message: 'Mover ratings are fetched successfully.',
            data: {
                ratings
            }
        });
    });
};

let getCurrentUser = (req, res, next) => {
    let userObj = userHelper.generateUserResponse(req.user);
    userObj.dateOfBirth = commonLib.formatDate(userObj.dateOfBirth);

    return responseModule.successResponse(res, {
        success: 1,
        message: 'User is logged in.',
        data: userObj
    });
};

let updateUserProfile = (req, res, next) => {
    let profileImage = req.body.profileImage,
        firstName = req.body.firstName,
        lastName = req.body.lastName,
        email = req.body.email,
        address = req.body.address || '',
        state = req.body.state || '',
        city = req.body.city || '',
        postalCode = req.body.postalCode || '',
        name = firstName + ' ' + lastName,
        phoneNumber = req.body.phoneNumber,

        filter = { _id: req.user._id };

    let updateObj = {
        firstName: firstName,
        lastName: lastName,
        name: name,
        profileImage: profileImage,
        phoneNumber: phoneNumber,
        email: email,
        address: address,
        city: city,
        state: state,
        postalCode: postalCode
    };

    if ( !profileImage ) {
        delete updateObj.profileImage;
    }

    if ( req.body.gender ) {
        updateObj.gender = req.body.gender;
    }

    if ( req.body.dateOfBirth ) {
        updateObj.dateOfBirth = commonLib.getUTCDateTime(req.body.dateOfBirth);
    }

    return userAccounts.findOneAndUpdate(filter, { $set: updateObj }, { new: true }).then(updatedUser => {
        if ( updatedUser ) {
            logController.createLog('info', 'normal', req.user.firstName + ' ' + req.user.lastName + ' updated his profile successfully', req.user.userType, req.user._id, {}, req.clientIp);

            let userObj = userHelper.generateUserResponse(updatedUser);
            userObj.dateOfBirth = commonLib.formatDate(userObj.dateOfBirth);

            return responseModule.successResponse(res, {
                success: 1,
                message: 'User profile info is updated successfully.',
                data: userObj
            });
        } else {
            logController.createLog('error', 'high', req.user.firstName + ' ' + req.user.lastName + ' tried to update his profile but failed.', req.user.userType, req.user._id, {}, req.clientIp);
            return next({ msgCode: 5049 });
        }
    }).catch(err => {
        logController.createLog('error', 'high', req.user.firstName + ' ' + req.user.lastName + ' tried to update his profile but failed.', req.user.userType, req.user._id, {}, req.clientIp);
        return next({ msgCode: 5049 });
    });
};

/* ****************************************************** Facebook Login *********************************************************/
let facebookLogIn = (req, res, next) => {
    try {
        passport.authenticate('facebook-token', { scope: [ 'email' ] }, (err, user, info) => {
            if ( err ) {
                winston.error(err);
                return next({ msgCode: 5058 });
            }
            if ( !user ) {
                return next(info);
            }
            // req.logIn(user, (err) => {
            //     if ( err ) {
            //         return next({ msgCode: 5055 });
            //     }
            //     return next();
            // });
            if ( user.facebookPhoneNumberUpdate ) {
                let userObj = userHelper.generateUserResponse(user);
                userObj.dateOfBirth = commonLib.formatDate(userObj.dateOfBirth);
                return responseModule.successResponse(res, {
                    success: 1,
                    message: 'Facebook user created successfully.',
                    data: userObj ? userObj : {}
                });
            } else {
                req.body.phoneNumber = user.phoneNumber;
                return next();
            }
        })(req, res, next);
    } catch ( err ) {
        return next({ msgCode: 5058 });
    }
};

let facebookProfileUpdateAndSendVerificationCode = (req, res, next) => {
    let email = req.body.email,
        phoneNumber = req.body.phoneNumber,

        filter = { facebookId: req.body.facebookId };

    let updateObj = { phoneNumber: phoneNumber, facebookPhoneNumberUpdate: false };

    if ( req.body.hasOwnProperty('email') ) {
        updateObj.email = email;
        updateObj.facebookEmailUpdate = false;
    }

    return userAccounts.findOneAndUpdate(filter, { $set: updateObj }, { new: true }).then(updatedUser => {
        if ( updatedUser ) {
            logController.createLog('info', 'normal', 'User updated his facebook profile successfully', 'user', req.body.facebookId, {}, req.clientIp);
            return next();
        } else {
            logController.createLog('error', 'high', 'User tried to update his facebook profile but failed.', 'user', req.body.facebookId, {}, req.clientIp);
            return next({ msgCode: 5049 });
        }
    }).catch(err => {
        logController.createLog('error', 'high', 'User tried to update his profile but failed.', 'user', req.body.facebookId, { err: (err.TypeError) ? err.TypeError : err }, req.clientIp);
        return next({ msgCode: 5049 });
    });
};

let updateAppVersion = (req, res, next) => {

    let filter = { _id: req.user._id };
    let updatedField = (req.user.deviceType === 'ios') ? 'iosAppVersion' : (req.user.deviceType === 'android') ? 'androidAppVersion' : '';
    let updated = { $set: { [updatedField]: _.trim(req.body.appVersionCode) } };
    if ( req.user.userType === 'user' ) {
        return userAccounts.findOneAndUpdate(filter, updated, { new: true }).then(updatedUser => {
            if ( updatedUser ) {
                return responseModule.successResponse(res, {
                    success: 1,
                    message: 'User is logged in.',
                    data: { versionUpdated: true }
                });
            } else {
                return next({ msgCode: 5064 });
            }
        }).catch(err => {
            logController.createLog('error', 'high', 'User tried to update his app version.', 'user', req.user._id, { err: (err.TypeError) ? err.TypeError : err }, req.clientIp);
            return next({ msgCode: 5064 });
        });

    } else if ( req.user.userType === 'mover' ) {

        return moverAccount.findOneAndUpdate(filter, updated, { new: true }).then(updatedUser => {
            if ( updatedUser ) {
                return responseModule.successResponse(res, {
                    success: 1,
                    message: 'User app version is updated.',
                    data: { versionUpdated: true }
                });
            } else {
                return next({ msgCode: 5064 });
            }
        }).catch(err => {
            logController.createLog('error', 'high', 'Mover tried to update his app version.', 'mover', req.user._id, { err: (err.TypeError) ? err.TypeError : err }, req.clientIp);
            return next({ msgCode: 5064 });
        });
    } else {
        return next({ msgCode: '0004' });
    }
};

let getSettings = (req, res, next) => {
    return settings.findOne({}, { maxNumberOfMover: 1, maxNumberOfRooms: 1 }).lean().then(setting => {
        if ( setting ) {
            delete setting._id;
            responseModule.successResponse(res, {
                success: 1,
                message: 'General Settings fetched successfully.',
                data: { setting: setting }
            });
        } else {
            responseModule.successResponse(res, {
                success: 1,
                message: 'General Settings fetched successfully.',
                data: { setting: { maxNumberOfMover: 10, maxNumberOfRooms: 10 } }
            });
        }
    }).catch((err) => {
        logController.createLog('error', 'high', 'user tried to get app settings.', 'user', req.user._id, { err: (err.TypeError) ? err.TypeError : err }, req.clientIp);
        return next({ msgCode: 70002 });
    });
};

module.exports = {
    logInUser,
    registerForPushNotification,
    userLoginSuccess,
    sendSignUpVerification,
    confirmAccount,
    getCurrentUser,
    resendVerificationCode,
    logOutAccount,
    mediaUploaded,
    sendLoginCode,
    fetchMovers,
    fetchSuggestedMovers,
    fetchMoverProfile,
    fetchMoverRatings,
    updateUserProfile,
    facebookLogIn,
    facebookProfileUpdateAndSendVerificationCode,
    updateAppVersion,
    getSettings,
};