/**
 * Created by Waqar on 1/5/2018.
 */

const winston = require('winston'),
    _ = require('lodash');

let validationResponse = (message, req, next) => {
    return req.getValidationResult().then((result) => {
        if ( !result.isEmpty() ) {
            let errors = result.array().map((error) => {
                return error.message;
            });
            winston.error(message + errors.join(' && '));
            return next({ msgCode: errors[ 0 ] });
        } else {
            return next();
        }
    });
};

let logInValidate = (req, res, next) => {
    req.assert('phoneNumber', 5005).notEmpty();
    req.assert('loginCode', 5033).notEmpty();

    validationResponse('user could not be logged in', req, next);
};

let validateSignUpParams = (req, res, next) => {
    req.body.email = req.body.email.toLowerCase();

    req.assert('firstName', 5003).notEmpty();
    req.assert('lastName', 5003).notEmpty();
    req.assert('email', 5000).notEmpty();
    req.assert('email', 5001).isEmail();
    req.assert('phoneNumber', 5005).notEmpty();
    req.assert('phoneNumber', 5006).isPhoneNumberValid();
    req.assert('deviceType', 5008).notEmpty();
    req.assert('deviceType', 5008).isValidDeviceType();
    req.assert('profileImage', 5047).isValidUrl();
    // req.assert('gender', 5048).isValidGenderType();

    validationResponse('User could not be signed up', req, next);
};

let confirmAccountValidate = (req, res, next) => {
    req.assert('phoneNumber', 5005).notEmpty();
    req.assert('confirmationCode', 5011).notEmpty();
    req.assert('deviceType', 5008).notEmpty();
    req.assert('deviceType', 5008).isValidDeviceType();
    req.assert('deviceToken', 2020).notEmpty();
    validationResponse('user could not perform confirm account action.', req, next);
};

let resendCodeValidate = (req, res, next) => {
    req.assert('phoneNumber', 5005).notEmpty();

    validationResponse('user could not perform resend code action.', req, next);
};

let moverDetailValidation = (req, res, next) => {
    req.assert('id', 5035).notEmpty();

    validationResponse('user could not fetch mover details.', req, next);
};

let userProfileValidation = (req, res, next) => {
    req.body.email = req.body.email.toLowerCase();

    req.assert('profileImage', 5047).isValidUrl();
    req.assert('firstName', 5003).notEmpty();
    req.assert('lastName', 5003).notEmpty();
    req.assert('email', 5000).notEmpty();
    req.assert('email', 5001).isEmail();
    req.assert('phoneNumber', 5005).notEmpty();
    req.assert('phoneNumber', 5006).isPhoneNumberValid();
    // req.assert('gender', 5048).isValidGenderType();

    validationResponse('user could not perform update profile info action.', req, next);
};

let facebookLogInValidate = (req, res, next) => {
    req.assert('access_token', 5059).notEmpty();
    // req.assert('deviceType', 5008).notEmpty();
    // req.assert('deviceType', 5008).isValidDeviceType();
    // req.assert('deviceToken', 2020).notEmpty();

    validationResponse('User could not perform facebook login.', req, next);
};

let facebookProfileValidate = (req, res, next) => {
    req.assert('phoneNumber', 5005).notEmpty();
    req.assert('facebookId', 5061).notEmpty();
    if ( req.body.hasOwnProperty('email') ) {

        req.assert('email', 5000).notEmpty();
        req.assert('email', 5001).isEmail();
    }

    validationResponse('User could not perform facebook profile Update:', req, next);
};

let userAppVersionValidation = (req, res, next) => {
    req.assert('appVersionCode', 5062).notEmpty();
    req.assert('appVersionCode', 5063).isNumber();

    validationResponse('User could not perform app version validation:', req, next);
};

module.exports = {
    logInValidate,
    validateSignUpParams,
    confirmAccountValidate,
    resendCodeValidate,
    moverDetailValidation,
    userProfileValidation,
    facebookLogInValidate,
    facebookProfileValidate,
    userAppVersionValidation
};
