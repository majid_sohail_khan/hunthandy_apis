/**
 * Created by Waqar on 05/01/2017.
 */


const userMiddleWare = require('../middlewares/user_accounts.middleware'),
    userController = require('../controller/user_accounts.controller'),
    passport = require('../../../../config/passport'),
    multer = require('../../../../config/multer'),
    commonLib = require('../../globals/global.library'),
    imageUpload = multer.upload(config.aws.s3.profileImageDirectory);

module.exports = (app, version) => {
    let moduleName = '/user';

    app.post(
        version + moduleName + '/uploadFile',
        imageUpload.array('image', 1),
        userController.mediaUploaded
    );
    app.post(
        version + moduleName + '/signUp',
        userMiddleWare.validateSignUpParams,
        userController.sendSignUpVerification
    );
    app.post(
        version + moduleName + '/verifyAccount',
        userMiddleWare.confirmAccountValidate,
        userController.confirmAccount
    );
    app.post(
        version + moduleName + '/resendConfirmationCode',
        userMiddleWare.resendCodeValidate,
        userController.resendVerificationCode
    );
    app.post(
        version + moduleName + '/sendLoginCode',
        userMiddleWare.resendCodeValidate,
        userController.sendLoginCode
    );
    app.post(
        version + moduleName + '/logIn',
        userMiddleWare.logInValidate,
        userController.logInUser,
        commonLib.enForceSingleSession,
        userController.registerForPushNotification,
        userController.userLoginSuccess
    );
    app.get(
        version + moduleName + '/logout',
        passport.isAuthenticated,
        userController.logOutAccount
    );
    app.post(
        version + moduleName + '/fetch/movers',
        passport.isAuthenticated,
        passport.isAuthorized('user'),
        userController.fetchMovers
    );
    app.post(
        version + moduleName + '/fetch/suggested/movers',
        passport.isAuthenticated,
        passport.isAuthorized('user'),
        commonLib.getSystemSettings,
        userController.fetchSuggestedMovers
    );
    app.get(
        version + moduleName + '/fetch/mover/:id',
        passport.isAuthenticated,
        passport.isAuthorized('user'),
        userMiddleWare.moverDetailValidation,
        userController.fetchMoverProfile
    );
    app.get(
        version + moduleName + '/fetch/ratings/mover/:id',
        passport.isAuthenticated,
        passport.isAuthorized('user'),
        userMiddleWare.moverDetailValidation,
        userController.fetchMoverRatings
    );
    app.get(
        version + moduleName + '/currentUser',
        passport.isAuthenticated,
        passport.isAuthorized('user'),
        userController.getCurrentUser
    );
    app.post(
        version + moduleName + '/update/profile',
        passport.isAuthenticated,
        passport.isAuthorized('user'),
        userMiddleWare.userProfileValidation,
        commonLib.fetchIPAdress,
        userController.updateUserProfile
    );

    app.post(
        version + moduleName + '/auth/facebook',
        userMiddleWare.facebookLogInValidate,
        commonLib.fetchIPAdress,
        userController.facebookLogIn,
        userController.sendLoginCode
    );

    app.post(
        version + moduleName + '/facebook/profile/update',
        userMiddleWare.facebookProfileValidate,
        commonLib.fetchIPAdress,
        userController.facebookProfileUpdateAndSendVerificationCode,
        userController.sendLoginCode
    );

    app.post(
        version + moduleName + '/update/version',
        passport.isAuthenticated,
        commonLib.fetchIPAdress,
        userMiddleWare.userAppVersionValidation,
        userController.updateAppVersion
    );

    app.get(
        version + moduleName + '/settings',
        passport.isAuthenticated,
        commonLib.fetchIPAdress,
        userController.getSettings
    );
};