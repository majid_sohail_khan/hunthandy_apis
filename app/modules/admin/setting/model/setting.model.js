/**
 * Created by Muhammad Waqar on 25/01/2018.
 */
var mongoose = require('mongoose'),
    timestamps = require('mongoose-timestamp');

var Schema = mongoose.Schema;

var SettingsSchema = new Schema({
    userCompanyPercentage: { type: Number, required: true },
    spCompanyPercentage: { type: Number, required: true },
    sameDayCancellationCharges: { type: Number, required: true },
    twentyFourHourCancellationCharges: { type: Number, required: true },
    fortyFourHourCancellationCharges: { type: Number, required: true },
    jobOffersTimeInMinutes: { type: Number, required: true },
    maxJobsCancelPerDay: { type: Number, required: true },
    maxNumberOfMover: { type: Number, required: true },
    maxNumberOfRooms: { type: Number, required: true },
    moverCancellationFee: { type: Number, required: true },
});

SettingsSchema.plugin(timestamps);

const SettingObject = mongoose.model('settings', SettingsSchema);

// seed documents
let Setting = [ {
    userCompanyPercentage: 30,
    spCompanyPercentage: 10,
    sameDayCancellationCharges: 50,
    twentyFourHourCancellationCharges: 30,
    fortyFourHourCancellationCharges: 10,
    jobOffersTimeInMinutes: 60,
    maxJobsCancelPerDay: 2,
    maxNumberOfMover: 10,
    maxNumberOfRooms: 10,
    moverCancellationFee: 10,
} ];

SettingObject.find({}, (err, settingObjects) => {
    if ( settingObjects.length < 1 ) {
        SettingObject.insertMany(Setting, (err) => {
            if ( err ) {
                winston.error(err);
            }
        });
    }
});

module.exports = SettingObject;