

const adminLoginController = require('../controller/login.controller');
const adminLoginMiddleware = require('../middleware/login.middleware');
const passport = require('../../../../../config/passport');

module.exports = (app, version) => {
    app.post(version + '/admin/logIn', adminLoginMiddleware.validateLogInParams, adminLoginController.adminLogInAccount);
    app.get(version + '/admin/logOut', passport.isAuthenticatedAdmin, adminLoginController.logOutAccount);
    app.get(version + '/admin/currentAccount', passport.isAuthenticatedAdmin, adminLoginController.getCurrentAccount);
};