const passport = require('passport');

// const mongoose = require('mongoose');
// const AdminUser = mongoose.model('AdminUser');


let adminLogInAccount = (req, res, next) => {
    passport.authenticate('Admin', (err, acct, info) => {
        if ( err ) {
            return next(err);
        }
        if ( !acct ) {
            return next(info);
        }
        req.logIn(acct, (err) => {
            if ( err ) {
                return next(err);
            }
            return res.json({
                success: 1,
                response: 200,
                data: {
                    // account: lib.createAccountObjectForResponse(acct)
                    account: acct
                }
            });
        });
    })(req, res, next);

};

let logOutAccount = (req, res, next) => {
    req.session.destroy(function (err) {
        if ( err ) return next(err);
        req.logOut();
        return res.json({
            success: 1,
            response: 200,
            message: 'User has been logout successfully.',
            data: {}
        });
    });
};
let getCurrentAccount = (req, res, next) => {
    return res.json({
        success: 1,
        response: 200,
        data: { account: req.user ? req.user : {} }
    });
};

module.exports = {
    adminLogInAccount,
    logOutAccount,
    getCurrentAccount
};