/**
 * Created by Raza on 7/31/2017.
 */
var mongoose = require('mongoose'),
    timestamps = require('mongoose-timestamp'),
    bcrypt = require('bcryptjs'),
    SALT_WORK_FACTOR = 10,
    winston = require('winston');

var Schema = mongoose.Schema;

var AdminSchema = new Schema({
    name: { type: String, required: true },
    email: { type: String, required: true, index: { unique: true } },
    password: { type: String, required: true },
    userType: { type: String, required: true, enum: ['Admin', 'SubAdmin'], default: 'Admin' },
    profileImageUrl: { type: String, default: null },
    mobileNumber: String,
});

AdminSchema.plugin(timestamps);

AdminSchema.index({ email: 1 });
AdminSchema.pre('save', function (next) {
    var user = this;

    // only hash the password if it has been modified (or is new)
    if (!user.isModified('password')) return next();

    // generate a salt
    bcrypt.genSalt(SALT_WORK_FACTOR, function (err, salt) {
        if (err) return next(err);

        // hash the password using our new salt
        bcrypt.hash(user.password, salt, function (err, hash) {
            if (err) return next(err);

            // override the cleartext password with the hashed one
            user.password = hash;
            next();
        });
    });
});

AdminSchema.methods.comparePassword = function (candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.password, function (err, isMatch) {
        if (err) return cb(err);
        cb(null, isMatch);
    });
};

// seed documents
const AdminUser = mongoose.model('AdminUser', AdminSchema);

AdminUser.find({}, (err, aUsers) => {
    if (!aUsers.length) {
        let superAdmin = {
            name: 'Super Admin',
            email: 'admin@movebig.co',
            password: 'admin1234',
        };

        let aUser = new AdminUser(superAdmin);
        aUser.save((err) => {
            if (err) {
                winston.error(err);
            }
        });
    }
});

module.exports = AdminUser;