/* const passport = require('passport');

const mongoose = require('mongoose');
const AdminUser = mongoose.model('AdminUser');*/

let validateLogInParams = (req, res, next) => {
    req.body.userId = req.body.userId;
    req.assert('email', 'Please enter mobile phone or email address.').notEmpty();
  //  req.assert('userId', 'Please enter a valid mobile phone/email address or password.').isUserIdValid();
    req.assert('password', 'Please enter password').notEmpty();

    const errors = req.validationErrors();

    if ( errors ) {
      //  winston.error('user could not be logged in', errors[ 0 ]);
        return next(errors[ 0 ]);
    }

    return next();
};

module.exports = {
    validateLogInParams
};