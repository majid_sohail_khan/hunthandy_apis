/**
 * Created by Asif on 7/5/2017.
 */

'use strict'; // NOSONAR

let successResponse = (res, data) => {
    data.response = 200;
    res.json(data);
};

let errorResponse = (res, err) => {

    res.json({
        success: 0,
        data: {},
        message: err,
        response: 304
    });

};


module.exports = {
    successResponse,
    errorResponse
};
