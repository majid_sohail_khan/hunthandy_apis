/**
 * Module dependencies.
 */

var mongoose = require('mongoose');
var LocalStrategy = require('passport-local').Strategy;
var User = mongoose.model('User');

/**
 * Expose
 */

module.exports = new LocalStrategy({
        usernameField: 'phoneNumber',
        passwordField: 'loginCode'
    },
    function (phoneNumber, loginCode, done) {
        var options = {
            criteria: { phoneNumber: phoneNumber }
        };
        User.load(options, function (err, user) {
            if (err) return done(err);
            if (!user) {
                return done(null, false, { message: 'Unknown user' });
            }
            if (!user.authenticate(loginCode)) {
                return done(null, false, { message: 'Invalid login code' });
            }
            return done(null, user);
        });
    }
);
