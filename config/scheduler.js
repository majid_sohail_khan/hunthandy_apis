const schedule = require('node-schedule');
const moveBigWeeksCron = require('../app/modules/crons/weeks.cron'),
    extraTimeCron = require('../app/modules/crons/extraTimeNotification.cron');

//schedule.scheduleJob('0 0 * * *', moveBigWeeksCron.checkWeeksIsExist); // run on every day
//schedule.scheduleJob('*/10 * * * *', moveBigWeeksCron.removeUnAssignedJobs); // run on every 10 Minutes
//schedule.scheduleJob('*/30 * * * * *', moveBigWeeksCron.checkTimeOutJobOffer); // run on every 30 seconds
//schedule.scheduleJob('*/10 * * * *', moveBigWeeksCron.checkVicTigOrders); // run on every 10 Minutes
//schedule.scheduleJob('*/2 * * * *', extraTimeCron.extraTimeRequestCron); // run on every 2 Minutes
//schedule.scheduleJob('*/1 * * * *', extraTimeCron.sendFinishedJobEstimatedTimeCron); // run on every One Minutes
//schedule.scheduleJob('*/30 * * * *', extraTimeCron.removedUnAcceptedJobs); // run on every 30 Minutes
//schedule.scheduleJob('0 0 * * *', extraTimeCron.updateMoverStripePayout); // run on every Day
