/**
 * Created by Asif on 7/14/2017.
 */

const winston = require('winston'),
    Agenda = require('agenda'),
    mongoose = require('mongoose'),
    chalk = require('chalk'),
    PN = mongoose.model('pushNotifications'),
    userAccounts = mongoose.model('userAccounts'),
    contractorAccounts = mongoose.model('contractorAccounts'),
    notifications = require('./notifications'),
    _ = require('lodash');

let agenda = new Agenda({
    db: {
        address: config.mongodb.host + config.mongodb.db_name
    }
});

agenda.on('ready', function () {
    winston.info('Agenda Started.');
    agenda.start();
});


// Push Notifications Job
agenda.define('pushNotification', {
    priority: 'high'
}, function (job) {
    // winston.info(chalk.green('push job received'));

    const information = job.attrs.data.notificationInfo,
        type = job.attrs.data.type;    // 1 for general Notification 2 for message Notifications

    if ( type === 1 ) {

        let messageType = information.messageType || '',
            userId = information.userId || '123',
            userType = information.userType || '123',
            message = information.message || 'MoveBig',
            senderName = information.senderName || 'MoveBig',
            badge = information.badge || 0,
            resource = (information.resource) ? information.resource : information.resource || '',
            status = information.statusCase || '';

        winston.info(chalk.green('Agenda triggered for General notification for user: ' + userId));
        let $filter = { _id: userId };
        if ( userType === 'user' ) {
            userAccounts.findOne($filter).then((account) => {
                if ( account && account.snsToken ) {
                    notifications.sendPush(account.snsToken, message, messageType, messageType, senderName, badge, resource, status);
                }
            });
        } else if ( userType === 'mover' ) {
            contractorAccounts.findOne($filter).then((account) => {
                if ( account && account.snsToken ) {
                    notifications.sendPush(account.snsToken, message, messageType, messageType, senderName, badge, resource, status);
                }
            });
        }
    } else if ( type === 2 ) {

        let messageType = information.messageType || '',
            userId = information.userId || '123',
            userType = information.userType || '123',
            message = information.message || 'MoveBig',
            senderName = information.senderName || 'MoveBig',
            badge = information.badge || 0,
            resource = (information.resource) ? information.resource : '',
            status = information.statusCase || '';

        winston.info(chalk.green('Agenda triggered for Messaging notification for user: ' + userId));

        let $filter = { _id: userId };
        if ( userType === 'user' ) {
            userAccounts.findOne($filter).then((account) => {
                if ( account && account.snsToken ) {
                    notifications.sendPush(account.snsToken, message, messageType, messageType, senderName, badge, resource, status);
                }
            });
        } else if ( userType === 'mover' ) {
            contractorAccounts.findOne($filter).then((account) => {
                if ( account && account.snsToken ) {
                    notifications.sendPush(account.snsToken, message, messageType, messageType, senderName, badge, resource, status);
                }
            });
        }
    }
});

agenda.on('success:pushNotification', function (job) {
    winston.info(chalk.green('push sending success to ' + job.attrs.data.notificationInfo.userId));
    job.remove();
});

agenda.on('fail:pushNotification', function (job) {
    winston.info(chalk.red('Push sending failed to user ' + job.attrs.data.notificationInfo.userId));
});