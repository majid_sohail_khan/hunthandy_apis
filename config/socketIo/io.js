/**
 * Created by Raza on 17/8/2017.
 */
'use strict'; //NOSONAR

global.io = null;
var server, adapter;

const winston = require('../winston'),
    socketio = require('socket.io'),
    cookieParser = require('cookie-parser'),
    passport = require('passport'),
    passportSocketIo = require("passport.socketio"),
    session = require('express-session'),
    mongoose = require('mongoose'),
    mongoAdapter = require('socket.io-adapter-mongo'),
    mongoStore = require('connect-mongo')(session);

module.exports = function (server) {

    winston.info('Launching Socket.io server...');

    global.io = socketio(server);
    
    adapter = mongoAdapter(config.mongodb.uri);
    io.adapter(adapter);

    io.use(passportSocketIo.authorize({
        cookieParser: cookieParser,    // the same middleware you registrer in express
        key: 'connect.sid',  //connect.sid
        secret: config.session.secret,
        store: new mongoStore({url: config.mongodb.uri}),
        resave: true,
        saveUninitialized: true,
        cookie: {
            maxAge: 100 * 24 * 3600 * 1000,
        },
        success: onAuthorizeSuccess,  // *optional* callback on success
        fail: onAuthorizeFail     // *optional* callback on fail/error
    }));

    function onAuthorizeSuccess(data, accept) {
        winston.info('passport.socketio: accepting socket connection...');
        return accept();
    }

    function onAuthorizeFail(data, message, error, accept) {
        winston.error('Socket connection denied because: ' + message);
        return accept(new Error(message));
    }

    return io;
};

