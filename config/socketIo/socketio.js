'use strict';
const _ = require('lodash'),
    winston = require('../winston'),
    chalk = require('chalk'),
    socketHelper = require('../../app/modules/socketIo/socket.controller'),
    jsonKeys = require('../../app/modules/socketIo/socket_keys.json'),
    chatController = '';//require('../../app/modules/chat/controllers/chat.controller');

module.exports = function (socket) {
    const userId = socket.request.user._id;
    const userType = socket.request.user.userType;
    // let flag = parseInt(socket.request._query[ 'flag' ] ? socket.request._query[ 'flag' ] : 0);

    const s = chalk.green('socket.io[' + userId + ']');

    socketHelper.addGlobalUserSocket(userId, socket);

    winston.info(s + chalk.green('++++++++CONNECTED+++++++ uid: ') + userId + ' socketid: ' + socket.id + ' name: ' + socket.request.user.name);

    socketHelper.createRoom('userWeb', userId, socket);

    // socketHelper.emitToSocket('offerAccepted', socket.id, userId, 'test data');


    socket.on('disconnect', function () {
        winston.log('info', chalk.red('******************* User Disconnected ******************'));
        winston.log('info', s + chalk.red('++++++++++++++DISCONNECTED-+++++++++++') + ' userName : ' + socket.request.user.name + ' ' + socket.request.user._id);
        socketHelper.removeGlobalUserSocket(socket.id, socket.request.user._id);
    });

    // reconnection Failed
    socket.on('reconnect_failed', () => {
        winston.info('***************Reconnection Failed**********************');
        winston.info(chalk.red(socket.id));
        winston.info('***************Reconnection Failed**********************');
        socketHelper.removeGlobalUserSocket(socket.id, socket.request.user.id);
    });

    // connection restored
    socket.on('reconnect', () => {
        winston.info('***************Reconnection Success**********************');
        winston.info(chalk.red(socket.id));
        socketHelper.addGlobalUserSocket(socket.request.user.id, socket.id);
        winston.info('***************Reconnection Success**********************');
    });

    // connection restored
    socket.on('reconnecting', () => {
        winston.info('***************Reconnection Attempt**********************');
        winston.info(chalk.red(socket.id));
        winston.info('***************Reconnection Attempt**********************');
    });

    // Custom Sockets Methods
    socket.on(jsonKeys.locationUpdate, (data, acknowledgement) => {
        winston.info('***************Socket Location Update**********************');
        winston.log('debug', 'updatedLatLong' + JSON.stringify(data));
        if ( data.latLong ) {
            let userId = socket.request.user._id.toString();
            return socketHelper.updateMoverLocation(userId, data, (err, updatedMover) => {
                // Socket Acknowledgement
                if ( err ) {
                    winston.error(err);
                    acknowledgement(err);
                }
                if ( updatedMover ) {
                    acknowledgement({ socket: { resource: { locationUpdated: true, latLong: data.latLong } } });
                }
            });
        }
    });

    socket.on(jsonKeys.markAsRead, (data) => {
        winston.info('***************Socket Mark Message as Read Method**********************');
        chatController.markMessageAsRead(data.messageId, userId, userType, (err) => {
            if ( err ) {
                winston.err(err);
            }
        });
    });

    socket.on(jsonKeys.sendMessage, (data, acknowledgement) => {
        winston.info('***************Socket Send Message On Job**********************');
        const user = socket.request.user;
        chatController.sendMessageOnJobThread(user, data, (err, message) => {
            // Socket Acknowledgement
            if ( err ) {
                acknowledgement(err);
            }

            if ( message ) {
                winston.info('***************Socket Sending Acknowledgement for message**********************');
                acknowledgement({ socket: { resource: message } });
            }
        });
    });
};