let passport = require('passport'),
    LocalStrategy = require('passport-local').Strategy,
    FacebookTokenStrategy = require('passport-facebook-token'),
    randomstring = require('randomstring'),
    mongoose = require('mongoose'),
    path = require('path'),
    env = process.env.NODE_ENV || 'development',
    userAccounts = mongoose.model('userAccounts'),
    contractorAccounts = mongoose.model('contractorAccounts'),
    AdminUser = mongoose.model('AdminUser'),
    config = require(path.join(__dirname, 'env', env + '.json')),
    winston = require('winston');

// Local Login Strategy
passport.use('user', new LocalStrategy({
        usernameField: 'phoneNumber',
        passwordField: 'loginCode',
    }, (phoneNumber, loginCode, done) => {
        userAccounts.findOne({ phoneNumber: phoneNumber }, (err, user) => {
            if ( err ) {
                return done(err);
            }
            if ( !user ) {
                return done(null, false, { msgCode: '0001' });
            }

            if ( user.isBlocked ) {
                return done(null, false, { msgCode: '0007' });
            } else if ( user.loginCode == loginCode ) {
                return done(null, user);
            } else {
                return done(null, false, { msgCode: '0002' });
            }
        });
    }
));

passport.use('Admin', new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password'
    }, function (username, password, done) {
        AdminUser.findOne({ email: username }, (err, acct) => {
            if ( err ) {
                return done(err);
            }
            if ( !acct ) {
                return done(null, false, { msgCode: '0001' });
            }
            acct.comparePassword(password, (err, isMatch) => {
                if ( err ) {
                    return done(err);
                }
                if ( !isMatch ) {
                    return done(null, false, { msgCode: '0002' });
                }
                return done(null, acct);
            });
        });
    }
));

passport.use('mover', new LocalStrategy({
        usernameField: 'phoneNumber',
        passwordField: 'loginCode',
    }, (phoneNumber, loginCode, done) => {
        contractorAccounts.findOne({ phoneNumber: phoneNumber }, (err, mover) => {
            if ( err ) {
                return done(err);
            }
            if ( !mover ) {
                return done(null, false, { msgCode: '0001' });
            }

            if ( mover.isBlocked ) {
                return done(null, false, { msgCode: '0007' });
            } else if ( mover.adminRejected ) {
                return done(null, false, { msgCode: '0006' });
            } else if ( mover.loginCode == loginCode ) {
                return done(null, mover);
            } else {
                return done(null, false, { msgCode: '0002' });
            }
        });
    }
));

// Facebook Login Strategy
passport.use(new FacebookTokenStrategy({
    clientID: config.facebook.clientId, clientSecret: config.facebook.clientSecret,
    enableProof: false, profileFields: [ 'emails', 'name', 'id' ], passReqToCallback: true
}, function (req, token, refreshToken, profile, done) {
    try {
        process.nextTick(function () {
            if ( profile.emails[ 0 ].value ) {
                let email = profile.emails[ 0 ].value.toLowerCase();
                userAccounts.findOne({ $or: [ { 'facebookId': profile.id }, { email: email } ] }, function (err, user) {
                    if ( err )
                        return done({ msgCode: 5058 });
                    if ( user ) {
                        if ( !user.isFacebookAccount ) {
                            let filter = { _id: user._id };
                            let updatedFields = {
                                $set: {
                                    facebookId: profile.id,
                                    facebookToken: token,
                                    isFacebookAccount: true
                                }
                            };
                            userAccounts.findOneAndUpdate(filter, updatedFields, { new: true }, (err, updateUser) => {
                                if ( err ) return done({ msgCode: 5058 });
                                return done(null, updateUser);
                            });
                        } else {
                            return done(null, user);
                        }
                    } else {
                        let newUser = {
                            'facebookId': profile.id,
                            'facebookToken': token,
                            'name': profile.name.givenName + ' ' + profile.name.familyName,
                            'firstName': profile.name.givenName,
                            'lastName': profile.name.familyName,
                            'isFacebookAccount': true,
                            'phoneNumber': '',
                            'deviceType': req.body.deviceType,
                            'deviceToken': req.body.deviceToken,
                            'profileImage': profile.photos[ 0 ].value,
                            'facebookPhoneNumberUpdate': true,
                            'userType': 'user',
                        }, email = (profile.emails[ 0 ].value || '').toLowerCase();

                        return userAccounts.findOneAndUpdate({ email: email }, newUser, { upsert: true, new: true })
                            .then(userCreated => {
                                if ( userCreated ) {
                                    return done(null, userCreated);
                                } else {
                                    return done({ msgCode: 5058 });
                                }
                            }).catch(err => {
                                winston.error(err);
                                return next({ msgCode: 5058 });
                            });
                    }
                });
            } else {
                userAccounts.findOne({ 'facebookId': profile.id }, function (err, user) {
                    if ( err )
                        return done({ msgCode: 5058 });
                    if ( user ) {
                        return done(null, user);
                    } else {
                        let newUser = {
                            'facebookId': profile.id,
                            'facebookToken': token,
                            'name': profile.name.givenName + ' ' + profile.name.familyName,
                            'firstName': profile.name.givenName,
                            'lastName': profile.name.familyName,
                            'isFacebookAccount': true,
                            'phoneNumber': '',
                            'deviceType': req.body.deviceType,
                            'deviceToken': req.body.deviceToken,
                            'profileImage': profile.photos[ 0 ].value,
                            'facebookPhoneNumberUpdate': true,
                            'facebookEmailUpdate': true,
                            'userType': 'user',
                        }, email = (randomstring.generate(7) + '@dummy.com').toLowerCase();

                        return userAccounts.findOneAndUpdate({ email: email }, newUser, { upsert: true, new: true })
                            .then(userCreated => {
                                if ( userCreated ) {
                                    return done(null, userCreated);
                                } else {
                                    return done({ msgCode: 5058 });
                                }
                            }).catch(err => {
                                winston.error(err);
                                return next({ msgCode: 5058 });
                            });
                    }
                });
            }
        });
    } catch ( err ) {
        winston.error(err);
        return done({ msgCode: 5058 });
    }
}));

passport.serializeUser((acct, done) => {
    if ( acct && acct._id !== null ) {
        done(null, acct._id);
    } else {
        done({ msgCode: '0001' });
    }
});

passport.deserializeUser((_id, done) => {
    userAccounts.findById(_id, function (err, acct) {
        if ( acct ) {
            done(err, acct);
        } else {
            contractorAccounts.findById(_id).lean().then(function (acct) {
                if ( acct ) {
                    done(err, acct);
                } else {
                    AdminUser.findById(_id, function (err, acct) {
                        if ( acct ) {
                            done(err, acct);
                        }
                        else {
                            done(err, false);
                        }
                    });
                }
            }).catch((err) => {
                done(err);
            });
        }
    });
});

// passport middlewares
passport.isAuthenticated = (req, res, next) => {
    if ( req.isAuthenticated() ) {
        return next();
    }
    return next({ msgCode: '0003' });
};

passport.isAuthorized = (userType) => {
    return (req, res, next) => {
        if ( req.user.userType == userType ) {
            return next();
        }
        return next({ msgCode: '0004' });
    };
};

passport.isAuthenticatedAdmin = (req, res, next) => {
    if ( req.isAuthenticated() ) {
        return next();
    }

    return res.json({
        success: 0,
        response: 401,
        message: 'User not authenticated.',
        data: {},
    });
};
// passport middlewares
passport.isAuthenticatedUser = (req, res, next) => {
    if ( req.isAuthenticated() ) {
        return next();
    }
    return next({ msgCode: '7022' });
};

module.exports = passport;
