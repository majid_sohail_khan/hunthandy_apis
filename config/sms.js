const client = require('twilio')(
    config.twilio.account_sid,
    config.twilio.auth_token
);

const winston = require('winston');

exports.sendMessage = (messageObject, cb) => {
    client.messages.create({
        from: config.twilio.phone_number,
        to: messageObject.to,
        body: messageObject.text
    }, (err, message) => {
        if ( err ) {
            winston.error('There was error sending message to ' + messageObject.to);
            return cb(err);
        } else {
            winston.info('A text message has been successfully sent to ' + messageObject.to);
            return cb();
        }
    });
};
